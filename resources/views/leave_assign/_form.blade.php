<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 3/25/16
 * Time: 4:53 PM
 */
 ?>

@if(!empty(session()->has('message')))
    <div class="col-xs-12">
        <div class="alert alert-{{session('message')['type']}} fade in">
            <button class="close" data-dismiss="alert">
                ×
            </button>
            <i class="fa-fw fa fa-check"></i>
            <strong>Success</strong> {!! session('message')['msg'] !!}
        </div>
    </div>
@endif

<div class="form-group col-xs-12 {{ $errors->has('employee_id') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('employee_id','Employee',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-10">
        @if(!empty($id))
            <?php $employee_id = [$id->employee->id => $id->employee->name]  ?>
        @else
            <?php $employee_id = []  ?>
        @endif
        {!! Form::select('employee_id',$employee_id,null,['class'=>'form-control']) !!}
        {!! $errors->first('employee_id','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group col-xs-12 {{ $errors->has('year') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('year','Year',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('year',null,['class'=>'form-control','placeholder'=>'YYYY']) !!}
        {!! $errors->first('year','<span class="help-block">:message</span>') !!}
    </div>
</div>
<div class="col-xs-12">
    <table class="table table-striped table-bordered table-hover" id="simpledatatable">
        <thead>
        <tr>
            <th>SL</th>
            <th>Leave Type</th>
            <th>Allow Days</th>
        </tr>
        </thead>
        <tbody id="leaveTypes">
            @if(!empty($id))
                <tr>
                    <td>1</td>
                    <td>{!! form::select('leave_type_id',[$id->leave_type_id=>$id->leaveType->name],$id->leave_type_id,
                        ['class'=>'form-control','readonly' =>'readonly']) !!}</td>
                    <td>{!! Form::text('leave_days',null,['class'=>'form-control']) !!}</td>
                </tr>
            @else
                @forelse($leaveTypes as $key=>$leaveType)
                    @if($leaveType->is_earn)
                        @continue
                    @else
                    <tr data-isMaternity="{!! $leaveType->is_maternity !!}" data-id="{!! $leaveType->id !!}">
                        <td>{!! $key+1 !!}</td>
                        <td>{!! Form::select('leave_types[]',[$leaveType->id=>$leaveType->name],$leaveType->id,
                        ['class'=>'form-control','readonly' =>'readonly']) !!}</td>
                        <td>{!! Form::number('leave_days[]',null,['class'=>'form-control']) !!}</td>
                    </tr>
                    @endif
                    @empty
                    <tr>
                        <td colspan="3"><h2>Can't find any Leave Type</h2></td>
                    </tr>
                @endforelse
            @endif
        </tbody>
    </table>
    <br>
</div>

<div class="col-xs-12">
    <div class="col-xs-10 pull-right">
        <button type="submit" class="btn btn-blue">Save</button>
    </div>
{{--    <a href="{!! action('OrganizationController@index') !!}" class="btn btn-warning">Cancel</a>--}}
</div>


@section('footer_script')
    <script>
        getvalueForSelect2("select[name='employee_id']",'employees',['id','old_card_no'],[],'id','old_card_no');


        $("select[name='employee_id']").on('change', function () {
            var url = "{!! url('employee') !!}/"+$(this).val();
            var employee = getValue(url, function (r) {

                if (r['sex']=="Female") {
                    $('tr[data-isMaternity="1"]').show();
                }else {
                    $('tr[data-isMaternity="1"]').hide();
                }
            })
            setSerialNumberInTable('#leaveTypes');
        });


        function setSerialNumberInTable (selector) {
            $(selector + " tr:visible").each(function (i) {
                $(this).children('td:first-child').html(i+1);
            });
        }

        setSerialNumberInTable('#leaveTypes');
    </script>
@endsection