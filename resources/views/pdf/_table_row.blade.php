<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 1/25/17
 * Time: 9:42 PM
 */?>

<tr>
	<td>{!! $key !!}</td>
	<td>{!! $row->employee->name !!}</td>
	<td>{!! $row->employee->designation->name !!}</td>
	<td>{!! $row->employee->old_card_no !!}</td>
	<td>
		{!! $row->employee->designation->grade !!}
	</td>
	<td>
		{!! date('d-M-y',strtotime($row->employee->date_of_joining)) !!}
	</td>
	<td>{!! $row->basic_salary !!}</td>
	<td>
		{!! $row->hra !!}
	</td>
	<td>
		{!! $row->medical !!}
	</td>
	<td>{!! $row->conveyance !!}</td>
	<td>
		{!! $row->food_all !!}
	</td>
	<td> {!! $row->gross_salary !!}</td>
	<td>{!! $row->total_days !!}</td>
	<td>{!! $row->p_days !!}</td>
	<td>
		{{--*/ $holidays = App\HolidayChild::where('holiday_id',$row->employee->holiday->id)
				->whereMonth('date','=',$row->month)->count() /*--}}
		{!! $holidays !!}
	</td>
	<td> {!! $row->total_leave or 0 !!}</td>
	<td>{!! $row->payable_leave or 0 !!}</td>
	<td>{!! $row->without_pay_leave or 0 !!}</td>
	<td>{!! $row->payable_days !!}</td>
	<td>
		{!! $row->ab_days !!}</td>


	<td>{!! $row->ab_deduction !!}</td>
	{{--<td>N/A</td>--}}


	<td>{!! $row->attendance_bonus !!}</td>
	<td>{!! round($row->ot_rate,2) !!}</td>
	<td>{!! $row->ot_hours !!}</td>
	<td>{!! $row->ot_amount !!}</td>
	<td>{!! ((int) $row->payable_amount + (int) $row->revenue_stamp + (int) $row->advance) - (int) $row->ot_amount !!}</td>

	<td>{!! $row->advance !!}</td>
	<td>{!! $row->revenue_stamp !!}</td>
	<td>{!! $row->payable_amount - $row->ot_amount !!}</td>
	<td></td>
</tr>
