<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 3/25/16
 * Time: 8:37 PM
 */


/**
 * @param $action
 * @param $id
 * @param string $label
 * @param string $style
 * @param string $class
 * @return string
 * Delete Data
 */
function delete_data($action,$id, $label = 'Delete',$style='icon',$class='text-danger')
{
    $form = Form::open(['action' => [$action,$id], 'method' => 'delete', 'id'=>'deleted_form_'.$id, 'style'=>'display:inline']);
    if ($style == 'icon') {
        $form .= "<a href='#' class='confirm $class' data-id=\"deleted_form_$id\"><i class=\"fa fa-trash\"></i></a>";//Form::submit('',['class'=>'btn btn-danger']);
    }else{
        $form .= Form::submit($label,['class'=>'btn btn-danger']);
    }
    return $form.=Form::close();
}


function menu_generate($menus)
{
    function menu($menu)
    {
        $sub_menu = $menu->chield;
        $html = '';
        $html .= "<li><a href='$menu->url'  class='";
        if (!empty($sub_menu) && count($sub_menu)>0) {
            $html .= 'menu-dropdown';
        }
        $html .= "'>";
        if ($menu->icon) {
            $html .= "<i class='menu-icon $menu->icon'></i>";
        }
        $html .=$menu->title;
        $html .= "</a>";
        if (!empty($sub_menu) && count($sub_menu)>0) {
//            dd('sdf');
            $html .= "<ul class='submenu'>";
            foreach ($sub_menu as $s_menu) {
                $html .= menu($s_menu);
            }
            $html .="</ul>";
        }
        $html .= "</li>";

        return $html;
    }

    $datas = '';

    if (!empty($menus)) {
        foreach ($menus as $menu) {
            $datas .= menu($menu);
        }
    }
    return $datas;
}


function digitConvertToBangla($input)
{
    $bn_digits=array('০','১','২','৩','৪','৫','৬','৭','৮','৯');
    return str_replace(range(0, 9),$bn_digits, $input);
}


/**
 * @param $actualOutTime
 * @param $actualOT
 * @param int $subTime
 * @return array
 * Get None Commercial Time.
 */
function noneComponentTime($actualOutTime,$actualOT,$subTime = 2)
{
    $ot_hour = \Carbon\Carbon::parse($actualOT)->hour;
    $ac_out_time = \Carbon\Carbon::parse($actualOutTime)->subHours($ot_hour > $subTime? $ot_hour - $subTime:0)->toTimeString();
    $ac_ot = \Carbon\Carbon::parse($actualOT)->subHours($ot_hour > $subTime? $ot_hour - $subTime:0)->toTimeString();

    return [$ac_out_time, $ac_ot];
}
