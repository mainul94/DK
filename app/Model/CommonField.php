<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 3/24/16
 * Time: 8:22 PM
 */
namespace App\Model;

trait CommonField
{
    /**
     * return Created By
     * @return mixed
     */
    public function createdBy()
    {
        return $this->belongsTo('\App\User','created_by');
    }


    /**
     * return Created By
     * @return mixed
     */
    public function updateBy()
    {
        return $this->belongsTo('\App\User','updated_by');
    }


    /**
     * Relation with organization
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function organization()
    {
        return $this->belongsTo('App\Organization');
    }


    /**
     * Relation with Unit
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function unit()
    {
        return $this->belongsTo('App\Unit');
    }


    /**
     * Relation with Division
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function division()
    {
        return $this->belongsTo('App\Division');
    }


    /**
     * Relation with Department
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function department()
    {
        return $this->belongsTo('App\Department');
    }


    /**
     * Relation with Section
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function section()
    {
        return $this->belongsTo('App\Section');
    }


    /**
     * Relation with Line
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function line()
    {
        return $this->belongsTo('App\Line');
    }


    /**
     * Relation with designationGroup
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function designationGroup()
    {
        return $this->belongsTo('App\DesignationGroup');
    }


    /**
     * Relation with designation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function designation()
    {
        return $this->belongsTo('App\Designation');
    }


    /**
     * Relation with employeeCategory
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function employeeCategory()
    {
        return $this->belongsTo('App\EmployeeCategory');
    }


    /**
     * Relation with employeeType
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function employeeType()
    {
        return $this->belongsTo('App\EmployeeType');
    }


    /**
     * Relation with Holiday
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function holiday()
    {
        return $this->belongsTo('App\Holiday');
    }


    /**
     * Relation with probationPeriod
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function probationPeriod()
    {
        return $this->belongsTo('App\ProbationPeriod');
    }
}