<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect('login');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web','auth']], function () {
    Route::resource('organization','OrganizationController');
    Route::resource('menu','MenuController');
    Route::resource('unit','UnitController');
    Route::resource('division','DivisionController');
    Route::resource('department','DepartmentController');
    Route::resource('section','SectionController');
    Route::resource('line','LineController');
    Route::resource('designation-group','DesignationGroupController');
    Route::resource('designation','DesignationController');
    Route::resource('employee-category','EmployeeCategoryController');
    Route::resource('employee-type','EmployeeTypeController');
    Route::resource('probation-period','ProbationPeriodController');
    Route::resource('holiday-type','HolidayTypeController');
    Route::resource('holiday','HolidayController');
    Route::resource('holiday-child','HolidayChildController');
    Route::resource('employee','EmployeeController');
    Route::resource('leave-type','LeaveTypeController');
    Route::resource('leave-assign','LeaveAssignController');
    Route::resource('leave-register','LeaveRegisterController');
    Route::resource('salary-type','SalaryTypeController');
    Route::resource('salary-structure','SalaryStructureController');
    Route::resource('salary-setting','SalarySettingController');
    Route::resource('salary','SalaryController');
    Route::resource('attendance','AttendanceController');
    Route::resource('import-attendance','AttendanceImportController');
    Route::resource('setting','SettingController');
    Route::resource('user','UserController');
    Route::resource('error-log','ImportErrorController',['except' => [
        'create','show','store'
    ]]);
    Route::get('/api/getvalue/','SettingController@getValue');
    Route::get('/api/get-values/','SettingController@getValues');
    Route::post('/holiday-child-multi-entry/{holiday}','HolidayChildController@multipleInsert');
    Route::get('multi-attendance','AttendanceController@multiAttendance');
    Route::post('multi-attendance','AttendanceController@multiAttendanceSave');
    Route::get('multi-leave-assign','AttendanceController@multiLeaveAssign');
    Route::post('multi-leave-assign','AttendanceController@multiLeaveAssignSave');
    Route::get('emp-leave-avalable','LeaveRegisterController@avalableLeave');
    Route::get('import-employee','EmployeeController@viewEmployeeImport');
    Route::post('import-employee','EmployeeController@importEmployee');
    Route::get('multiple-leave-assign','LeaveAssignController@getMultiple');
    Route::post('multiple-leave-assign','LeaveAssignController@postMultiple');
    Route::get('get-salary-setting','SalarySettingController@getSalarySettingByDesignation');
});

Route::group(['middleware' => ['web','auth'],'prefix'=>'report'], function () {
    Route::get('salary/{comp?}','ReportController@salary');
    Route::get('salary-designation-wise','ReportController@salaryDesignation');
    Route::get('payslip','ReportController@salaryPayslip');
    Route::get('employee', 'ReportController@employee');
//    Attendance
    /**
     * Wite Max ot value as you want to see
     * Ex. /job-card/2 it's mean show maximum 2 hours
     * if /job-card/3 it's means show maximum 3 hours overtime
     */
    Route::get('attendance','ReportController@attendance');
    Route::get('attendance-summary', 'ReportController@attendanceSummary');
    Route::get('daily-attendance/{maxOt?}', ['as'=>'daily-attendance','uses'=>'ReportController@dailyAttendance']);
    Route::get('absent-list', ['as'=>'absent-list','uses'=>'ReportController@dailyAttendance']);
    Route::get('job-card/{maxOt?}', 'ReportController@jobCard');
    Route::get('id-card','ReportController@idCard');
//    Attendance
});

Route::get('pdf/salary-sheet/{month}/{year}', 'PDFController@salary');

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');
});
