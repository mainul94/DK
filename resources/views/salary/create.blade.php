<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 3/25/16
 * Time: 4:52 PM
 */
?>

@extends('layouts.layout')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-bottom bordered-blue">
                <span class="widget-caption">Genarate Salary</span>
            </div>
            <div class="widget-body">
                <div class="row">
                    <div class="col-sm-12">
                        {!! Form::open(['action'=>['SalaryController@store']]) !!}
                            <div class="form-group col-xs-4 {{ $errors->has('month') ? 'has-error':''}}">
                                <div class="col-sm-4 text-right">
                                    {!! Form::label('month','Month',['class'=>'field-required']) !!}
                                </div>
                                <div class="col-sm-8">
                                    {!! Form::select('month',$months,$month,['class'=>'form-control']) !!}
                                    {!! $errors->first('month','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="form-group col-xs-3 {{ $errors->has('year') ? 'has-error':''}}">
                                <div class="col-sm-4 text-right">
                                    {!! Form::label('year','Year',['class'=>'field-required']) !!}
                                </div>
                                <div class="col-sm-8">
                                    {!! Form::select('year',$years,$year,['class'=>'form-control']) !!}
                                    {!! $errors->first('year','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-xs-2">
                                <div class="col-xs-10 pull-right">
                                    <button type="submit" class="btn btn-blue">Genarate</button>
                                </div>
                            </div>
                        {!! Form::close() !!}


                        @if(!empty(session()->has('message')))
                            @foreach(session('message') as $message)
                                <div class="col-xs-12">
                                    <div class="alert alert-{{$message['type']}} fade in">
                                        <button class="close" data-dismiss="alert">
                                            ×
                                        </button>
                                        <i class="fa-fw fa fa-check"></i>
                                        <strong>Success</strong> {!! $message['msg'] !!}
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
