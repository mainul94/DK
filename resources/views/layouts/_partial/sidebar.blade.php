<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 3/25/16
 * Time: 3:49 PM
 */
?>


<!-- Page Sidebar -->
<div class="page-sidebar" id="sidebar">
    <!-- Page Sidebar Header-->
    <div class="sidebar-header-wrapper">
        <input type="text" class="searchinput" />
        <i class="searchicon fa fa-search"></i>
        <div class="searchhelper">Search Reports, Charts, Emails or Notifications</div>
    </div>
    <!-- /Page Sidebar Header -->
    <!-- Sidebar Menu -->
    <ul class="nav sidebar-menu">
        {!! menu_generate($menus) !!}
    </ul>
    <!-- /Sidebar Menu -->
</div>
<!-- /Page Sidebar -->
