<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 3/25/16
 * Time: 4:05 PM
 */
$sl = 1;
?>


@extends('layouts.layout')


@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="well with-header">
            <div class="header bg-info">
                <div class="row">
                    <div class="col-sm-7 col-xs-12">
                        Menu List
                    </div>
                </div>
            </div>

            <div class="col-xs-12 text-right padding-bottom-20px">
                <a href="{!! action('MenuController@create') !!}" class="btn btn-info">Create</a>
            </div>
            @if(!empty(session()->has('message')))
                <div class="col-xs-12">
                    <div class="alert alert-{{session('message')['type']}} fade in">
                        <button class="close" data-dismiss="alert">
                            ×
                        </button>
                        <i class="fa-fw fa fa-check"></i>
                        <strong>Success</strong> {!! session('message')['msg'] !!}
                    </div>
                </div>
            @endif
            @if(count($menus)>0)
            <table class="table table-hover table-bordered">
                <thead class="bordered-blue">
                <tr>
                    <th>Sl.No</th>
                    <th>Title</th>
                    <th>Slug</th>
                    <th>Icon</th>
                    <th>Url</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($menus as $menu)
                        <tr>
                            <td>{!! $sl !!} <?php $sl++; ?></td>
                            <td>{!! $menu->title !!}</td>
                            <td>{!! $menu->slug !!}</td>
                            <td>{!! $menu->icon !!}</td>
                            <td>{!! $menu->url !!}</td>
                            <td class="text-center">
                                <a href="{!! action('MenuController@edit',$menu->id) !!}" ><i class="fa fa-pencil-square-o text-danger"></i></a>
                                {!! delete_data('MenuController@destroy',$menu->id) !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            @else
                <h3 class="text-muted text-center">
                    No Data
                </h3>
            @endif
        </div>

    </div>
</div>
@endsection