<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 7/27/16
 * Time: 9:40 PM
 */
?>
@extends('layouts.layout')
@section('content')
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="widget">
                <div class="widget-header ">
                    <span class="widget-caption">Employee Job Card</span>
                </div>
                <div class="widget-body">
                    <div class="col-xs-12">
                        {!! Form::open(['action'=>['ReportController@jobCard',$maxOt],'method'=>'GET']) !!}
                        <div class="form-group col-xs-4 {{ $errors->has('month') ? 'has-error':''}}">
                            <div class="col-sm-4 text-right">
                                {!! Form::label('month','Date',['class'=>'field-required']) !!}
                            </div>
                            <div class="col-sm-8">
                                {!! Form::select('month',$months,$month,['class'=>'form-control']) !!}
                                {!! $errors->first('month','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="form-group col-xs-3 {{ $errors->has('year') ? 'has-error':''}}">
                            <div class="col-sm-4 text-right">
                                {!! Form::label('year','Year',['class'=>'field-required']) !!}
                            </div>
                            <div class="col-sm-8">
                                {!! Form::select('year',$years,$year,['class'=>'form-control']) !!}
                                {!! $errors->first('year','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        {{--<div class="form-group col-xs-3 {{ $errors->has('section_id') ? 'has-error':''}}">
                            <div class="col-sm-4 text-right">
                                {!! Form::label('section_id','Section') !!}
                            </div>
                            <div class="col-sm-8">
                                {!! Form::select('section_id',$sections,$section_id,['class'=>'form-control']) !!}
                                {!! $errors->first('section_id','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>--}}
                        <div class="form-group col-xs-4 {{ $errors->has('employee_id') ? 'has-error':''}}">
                            <div class="col-sm-4 text-right">
                                {!! Form::label('employee_id','Employee') !!}
                            </div>
                            <div class="col-sm-8">
                                {!! Form::select('employee_id',$employees,$employee_id,['class'=>'form-control']) !!}
                                {!! $errors->first('employee_id','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="col-xs-1">
                            <div class="col-xs-10 pull-right">
                                <button type="submit" class="btn btn-info">Filter</button>
                            </div>
                        </div>
                        {!! Form::close() !!}

                    </div>
                    <div class="clearfix"></div>
                    <div class="table-resposive reportForPrint">
                        <table class="table table-stript table-bordered" id="attendanceSummary">

                            <caption>
                                <div class="text-center">
                                   <h2><b>DK Global Fashion Ware Limited</b></h2>
								   <p>Molla Super Market,Beron,Jamgora,Ashulia,Saver,Dhaka</p>
								   <p><b>Individual Attendance/Overtime Report</b></p>
								   
                                   
                                </div>
                                <div class="col-xs-4">
                                    <p><strong>ID :</strong> {!! $employee->old_card_no or "" !!}</p>
                                    <p><strong>Section :</strong> {!! $employee->section->name or "" !!}</p>
                                </div>
                                <div class="col-xs-4">
                                    <p><strong>Name :</strong> {!! $employee->name or "" !!}</p>
                                    <p><strong>Designation :</strong> {!! $employee->designation->name or "" !!}</p>
                                </div>
                                <div class="col-xs-4">
                                    <p><strong>Line :</strong> {!! $employee->line->name or "" !!}</p>
                                    <p><strong>Grade :</strong> {!! $employee->designation->grade or "" !!}</p>
                                </div>
                            </caption>
                            <thead>
                            <tr>
                                <th style="text-align:center;">Date</th>
                                <th style="text-align:center;">Holiday</th>
                                <th style="text-align:center;">Leave Type</th>
                                <th style="text-align:center;">InTime</th>
                                <th style="text-align:center;">OutTime</th>
                                <th style="text-align:center;">OverTime</th>
                                <th style="text-align:center;">Remark</th>
                            </tr>
                            </thead>
                            @if(!empty($employee))
                            <tbody>
                                    {{--*/ $date= \Carbon\Carbon::create($year,$month,0); $holidays = 0; $leaves = 0; $absents = 0; $presents = 0; /*--}}
                                @for($day = 0; $day < $date->daysInMonth; $day++)
                                    {{--*/
                                    $date->addDay(1);
                                    $holiday = $employee->holiday->children->where('date',$date->toDateString())->first();
                                    $leave = \App\leaveRegister::where('employee_id',$employee->id)->where('p_start_date','<=',$date->toDateString())
                                    ->where('p_end_date','>=',$date->toDateString())->first();
                                    if (!empty($leave)){
                                    $leave = \App\leaveDetail::where('leave_register_id',$leave->id)->where('start_date','<=',$date->toDateString())
                                    ->where('end_date','>=',$date->toDateString())->first();
                                    }
                                    $attendance = $employee->attendance->where('date',$date->toDateString())->first();
                                    if(!is_null($attendance)) {
                                        $attendance->overtime = explode(':', $attendance->overtime)[0];
                                    }
                                    $holiday ? $holidays++:'';
                                    $leave ? $leaves++:'';
                                    (empty($holiday)&&empty($leave)&&empty($attendance)) || ($attendance && $attendance->status!="Present") ? $absents++:'';
                                    $attendance && $attendance->status=="Present" ? $presents++: '';
                                    if($attendance && intval($maxOt)){
                                        $noneComponentTime = noneComponentTime($attendance->out_time,$attendance->overtime,intval($maxOt));
                                    }
                                    /*--}}
                                    <tr style="font-size:15px;">
                                        <td>{!! $date->format('d/m/Y') !!}</td>
                                        <td>{!! $holiday->name or "" !!}</td>
                                        <td>{!! $leave->leaveType->name or "" !!}</td>
                                        <td>{!! $attendance->in_time or "" !!}</td>
                                        @if(intval($maxOt) && !empty($noneComponentTime))
                                            <td>{!! $noneComponentTime[0] or "" !!}</td>
                                            <td>{!! explode(':', $noneComponentTime[1])[0] or 0 !!}</td>
                                        @else
                                            <td>{!! $attendance->out_time or "" !!}</td>
                                            <td>{!! $attendance->overtime or 0 !!}</td>
                                        @endif
                                        <td>{!! $attendance->remark or ""!!}</td>
                                    </tr>
                                    @endfor
                            </tbody>
                            <tfoot>

                                <tr style="border-top: 5px solid lightcoral">
                                    {{-- TODO This Value came from Settings --}}
                                    <td colspan="3">Lunch Break: {{--*/ $settings= json_decode($settings);/*--}} {!! str_replace('-',' to ',$settings->lunch_break) !!}</td>
                                    <td colspan="2">Total OT Hours</td>
                                    <td colspan="2">{!! 0 !!}</td>
                                </tr>
                                <tr>
                                    <td>
                                        Month of Days   {!! $date->daysInMonth !!}
                                    </td>
                                    <td>Holiday {!! $holidays !!} Days</td>
                                    <td>Leave {!! $leaves !!} Days</td>
                                    <td>Present {!! $presents  !!}</td>
                                    <td>Absent {!! $absents !!}</td>
                                    <td colspan="2">Authorization Signature</td>
                                </tr>
                            </tfoot>
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_script')
    <script>
        getvalueForSelect2("select[name='section_id']",'sections',['id','name'],[],'id','name');
        getvalueForSelect2("select[name='employee_id']",'employees',['id','old_card_no'],[],'id','old_card_no');
    </script>
    <script src="{{ url('assets/js/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('assets/js/datatable/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ url('assets/js/datatable/dataTables.bootstrap.min.js') }}"></script>
    <script>
        var InitiateAttendanceSummaryReportTable = function() {
            return {
                init: function() {
                    var oTable = $('#attendanceSummary').dataTable({
                        "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                        // "aLengthMenu": [
                        //     [5, 15, 20, -1],
                        //     [5, 15, 20, "All"]
                        // ],
                        // "iDisplayLength": 10,
                        "oTableTools": {
                            "aButtons": [
                                "print"
                            ]
                        },
                        "language": {
                            "search": "",
                            "sLengthMenu": "_MENU_",
                            "oPaginate": {
                                "sPrevious": "Prev",
                                "sNext": "Next"
                            }
                        },
                        "searching": false,
                        "paging":   false,
                        "ordering": false,
                        "info":     false
                    });

                    /*$("tfoot input").keyup(function() {
                     /!* Filter on the column (the index) of this element *!/
                     oTable.fnFilter(this.value, $("tfoot input").index(this));
                     });*/
                    $('.DTTT_button_print').on('click',function (event) {
                        event.preventDefault();
                        window.print();
                    });
                }
            };
        }();
        InitiateAttendanceSummaryReportTable.init();
    </script>
@endsection
