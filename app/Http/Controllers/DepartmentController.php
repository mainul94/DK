<?php

namespace App\Http\Controllers;

use App\Department;
use Illuminate\Http\Request;

use App\Http\Requests;

class DepartmentController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rows = Department::all();
        return view('department.listview',compact('rows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('department.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'organization_id'=>'required',
            'unit_id'=>'required',
            'division_id'=>'required'
        ]);
        $values = $request->all();
        $values['created_by'] = $request->user()->id;
        Department::create($values);

        if ($request->ajax()) {
            return response()->json([
                'message'=>'Success'
            ]);
        }

        return redirect()->back()->with('message',['type'=>'success','msg'=>'Successfully Saved']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( Request $request,$id)
    {
        if ($request->ajax()) {
            return response()->json($id->toArray());
        }
        return view('department.view',compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('department.edit',compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id->fill($request->all());
        $id->fill(['updated_by'=>$request->user()->id]);
        $id->save();
        return redirect()->to(action('DepartmentController@edit',$id))->with('message',['type'=>'success','msg'=>'Successfully Updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id->delete();
        return redirect()->to(action('DepartmentController@index'))->with('message',['type'=>'danger','msg'=>'Successfully Deleted']);
    }
}
