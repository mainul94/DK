<?php

namespace App\Http\Controllers;

use App\Section;
use Illuminate\Http\Request;

use App\Http\Requests;

class SectionController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rows = Section::all();
        return view('section.listview',compact('rows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('section.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'organization_id'=>'required',
            'unit_id'=>'required',
            'division_id'=>'required',
            'department_id'=>'required'
        ]);
        $values = $request->all();
        $values['created_by'] = $request->user()->id;
        Section::create($values);

        if ($request->ajax()) {
            return response()->json([
                'message'=>'Success'
            ]);
        }

        return redirect()->back()->with('message',['type'=>'success','msg'=>'Successfully Saved']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show( Request $request,$id)
    {
        if ($request->ajax()) {
            return response()->json($id->toArray());
        }
        return view('section.view',compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('section.edit',compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id->fill($request->all());
        $id->fill(['updated_by'=>$request->user()->id]);
        $id->save();
        return redirect()->to(action('SectionController@edit',$id))->with('message',['type'=>'success','msg'=>'Successfully Updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id->delete();
        return redirect()->to(action('SectionController@index'))->with('message',['type'=>'danger','msg'=>'Successfully Deleted']);
    }
}
