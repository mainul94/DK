<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 7/24/16
 * Time: 9:00 PM
 */
?>
@extends('layouts.layout')

@section('content')

    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="widget">
                <div class="widget-header ">
                    <span class="widget-caption">Attendance Summary Report</span>
                </div>
                <div class="widget-body">
                    <div class="col-xs-12">
                        {!! Form::open(['action'=>['ReportController@attendanceSummary'],'method'=>'GET']) !!}
                        <div class="form-group col-xs-4 {{ $errors->has('date') ? 'has-error':''}}">
                            <div class="col-sm-4 text-right">
                                {!! Form::label('date','Date',['class'=>'field-required']) !!}
                            </div>
                            <div class="col-sm-8">
                                {!! Form::date('date',$date,['class'=>'form-control']) !!}
                                {!! $errors->first('date','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="form-group col-xs-3 {{ $errors->has('section_id') ? 'has-error':''}}">
                            <div class="col-sm-4 text-right">
                                {!! Form::label('section_id','Section') !!}
                            </div>
                            <div class="col-sm-8">
                                {!! Form::select('section_id',$sections,$section_id,['class'=>'form-control']) !!}
                                {!! $errors->first('section_id','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="form-group col-xs-4 {{ $errors->has('designation_id') ? 'has-error':''}}">
                            <div class="col-sm-4 text-right">
                                {!! Form::label('designation_id','Designation') !!}
                            </div>
                            <div class="col-sm-8">
                                {!! Form::select('designation_id',$designations,$designation_id,['class'=>'form-control']) !!}
                                {!! $errors->first('designation_id','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="col-xs-1">
                            <div class="col-xs-10 pull-right">
                                <button type="submit" class="btn btn-info">Filter</button>
                            </div>
                        </div>
                        {!! Form::close() !!}

                    </div>
                    <div class="clearfix"></div>
                    <div class="table-resposive reportForPrint">
                        <table class="table table-stript table-bordered" id="attendanceSummary">

                            <caption>
                                <div class="text-center">
                                   <h2>DK Global Fashion Ware Limited</h2>
                                    <p>Molla Super Market,Beron,Jamgora,Ashulia,Saver,Dhaka</p>
                                </div>
                                <p>
                                    Attendance Summary for the Date of : {{ date('d-M-Y',strtotime($date)) }}
                                    <span class="pull-right">
                                        {{ date('d/m/Y') }}
                                    </span>
                                </p>
                            </caption>
                            <thead>
                            <tr>
                                <th>Section</th>
                                <th>Designation</th>
                                <th>Total Man Power</th>
                                <th>Present</th>
                                <th>Absent</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($rows as $row)
                                <tr>
                                    <td>{!! $row->name !!}</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                @if($row->designations)
                                    {{--*/ $total = 0; $present = 0; $absent = 0;  /*--}}
                                    @foreach($row->designations as $designation)
                                        <tr>
                                            <td></td>
                                            <td>{!! $designation['name'] !!}</td>
                                            <td>{!! $designation['total'] !!}</td>
                                            <td>{!! $designation['present'] !!}</td>
                                            <td>{!! $designation['absent'] !!}</td>
                                        </tr>
                                    @endforeach

                                    <tr>
                                        <td></td>
                                        <td><strong>Section Total</strong></td>
                                        <td><strong>{!! $row->total !!}</strong></td>
                                        <td><strong>{!! $row->present !!}</strong></td>
                                        <td><strong>{!! $row->absent !!}</strong></td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_script')
    <script>
        getvalueForSelect2("select[name='section_id']",'sections',['id','name'],[],'id','name');
        getvalueForSelect2("select[name='designation_id']",'designations',['id','name'],[['section_id']],'id','name');
    </script>
    <script src="{{ url('assets/js/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('assets/js/datatable/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ url('assets/js/datatable/dataTables.bootstrap.min.js') }}"></script>
    <script>
        var InitiateAttendanceSummaryReportTable = function() {
            return {
                init: function() {
                    var oTable = $('#attendanceSummary').dataTable({
                        "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                        // "aLengthMenu": [
                        //     [5, 15, 20, -1],
                        //     [5, 15, 20, "All"]
                        // ],
                        // "iDisplayLength": 10,
                        "oTableTools": {
                            "aButtons": [
                                "print"
                            ]
                        },
                        "language": {
                            "search": "",
                            "sLengthMenu": "_MENU_",
                            "oPaginate": {
                                "sPrevious": "Prev",
                                "sNext": "Next"
                            }
                        },
                        "searching": false,
                        "paging":   false,
                        "ordering": false,
                        "info":     false
                    });

                    /*$("tfoot input").keyup(function() {
                     /!* Filter on the column (the index) of this element *!/
                     oTable.fnFilter(this.value, $("tfoot input").index(this));
                     });*/
                    $('.DTTT_button_print').on('click',function (event) {
                        event.preventDefault();
                        window.print();
                    });
                }
            };
        }();
        InitiateAttendanceSummaryReportTable.init();
    </script>
@endsection