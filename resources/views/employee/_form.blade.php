<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 3/25/16
 * Time: 4:53 PM
 */
 ?>

<div>
    <div class="widget-header bordered-bottom bordered-blue">
        <span class="widget-caption">Employee Form</span>
    </div>

@if(!empty(session()->has('message')))
    <div class="col-xs-12">
        <div class="alert alert-{{session('message')['type']}} fade in">
            <button class="close" data-dismiss="alert">
                ×
            </button>
            <i class="fa-fw fa fa-check"></i>
            <strong>Success</strong> {!! session('message')['msg'] !!}
        </div>
    </div>
@endif
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#basic_info" aria-controls="basic_info" role="tab" data-toggle="tab">Basic Information</a></li>
        <li role="presentation"><a href="#Personal_info" aria-controls="Personal_info" role="tab" data-toggle="tab">Personal Information</a></li>
        <li role="presentation"><a href="#org_info" aria-controls="org_info" role="tab" data-toggle="tab">Organizaton Information</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content" style="overflow: hidden">

        <div role="tabpanel" class="tab-pane active" id="basic_info">
            <div class="row">
                @include('employee._basic_form')
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="Personal_info">
            <div class="col-sm-8 col-sm-offset-1">
                @include('employee._personal_form')
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="org_info">
            <div class="col-sm-8 col-sm-offset-1">
                @include('employee._org_form')
            </div>
        </div>
    </div>

    {{--<div class="footer bordered-bottom bordered-blue">--}}
        {{--<span class="widget-caption">Employee Form</span>--}}
    {{--</div>--}}
    <div class="well with-header">
        <div class="header">
            <div class="col-xs-12">
                <div class="col-xs-10 pull-right">
                    <button type="submit" class="btn btn-blue">Save</button>
                </div>
                {{--    <a href="{!! action('OrganizationController@index') !!}" class="btn btn-warning">Cancel</a>--}}
            </div>
        </div>
    </div>

</div>


@section('footer_script')
    <script src="{{ asset('assets/js/datetime/bootstrap-datepicker.js') }}"></script>
    <script>
        getvalueForSelect2("select[name='organization_id']",'organizations',['id','name'],[],'id','name');
        getvalueForSelect2("select[name='designation_group_id']",'designation_groups',['id','name'],[],'id','name');
        getvalueForSelect2("select[name='unit_id']",'units',['id','name'],[['organization_id']],'id','name');
        getvalueForSelect2("select[name='division_id']",'divisions',['id','name'],[['organization_id'],['unit_id']],'id','name');
        getvalueForSelect2("select[name='department_id']",'departments',['id','name'],[['organization_id'],['unit_id'],['division_id']],'id','name');
        getvalueForSelect2("select[name='section_id']",'sections',['id','name'],[['organization_id'],['unit_id'],['division_id'],['department_id']],'id','name');
        getvalueForSelect2("select[name='line_id']",'lines',['id','name'],[['organization_id'],['unit_id'],['division_id'],['department_id'],['section_id']],'id','name');
        getvalueForSelect2("select[name='designation_id']",'designations',['id','name'],[],'id','name');
        getvalueForSelect2("select[name='employee_category_id']",'employee_categories',['id','name'],[],'id','name');
        getvalueForSelect2("select[name='employee_type_id']",'employee_types',['id','name'],[],'id','name');
        getvalueForSelect2("select[name='holiday_id']",'holidays',['id','name'],[],'id','name');
        getvalueForSelect2("select[name='probation_period_id']",'probation_periods',['id','name'],[],'id','name');
//        Personal
        getvalueForSelect2("select[name='holiday_id']",'holidays',['id','name'],[],'id','name');

//        Bank Info Show and Hide
        function hideAndShowONBasePaymentMOde(selectorValu) {

            console.log(selectorValu)
            if (selectorValu == "Bank") {
                $('input[name="bank_name"],input[name="branch_name"],input[name="account_holder"],input[name="account_no"]').parents('.form-group').show('slow');
            }else {
                $('input[name="bank_name"],input[name="branch_name"],input[name="account_holder"],input[name="account_no"]').val('').parents('.form-group').hide();
            }
        }

        $('select[name="payment_mode"]').on('change', function () {
            hideAndShowONBasePaymentMOde($(this).val());
        });
        hideAndShowONBasePaymentMOde("Cash")


//        Date Picker
//        var dateSet = $('[name="date_of_joining"],[name="date_of_separation"]').datepicker({
////            weekStart:0,
//            format:'yyyy-mm-dd'
//        });

//        Set Min Max Value on chnage designation
        $('[name="designation_id"]').on('change',function () {
            setMinMaxValue($(this).val())
        });
        function setMinMaxValue(designation) {
            if (typeof designation ==='undefined') {
                return
            }
            $.ajax({
                url:"{{ url('get-salary-setting') }}",
                dataType:"JSON",
                data:{'designation_id':designation},
                success:function (r) {
                    $.each(r,function (key,val) {
                        if (key.startsWith('min_') || key.startsWith('max_')) {
                            $('[name="'+key+'"]').val(val);
                            if (key in ['min_conveyance','medical']) {
                                $('[name="'+key.replace('min_','')+'"]').val(val);
                            }else if (key == 'min_food'){
                                $('[name="food_all"]').val(val);
                            }
                        }
                        else if (key == 'employee_type_id') {
                            getValue("{{ url('employee-type') }}/"+val,function (emptype) {
                                if (emptype) {
                                    $('[name="'+key+'"]').append('<option value="'+val+'" selected="selected">'+emptype.name+'</option>').change();
                                }
                            });
                        }
                        else if (key == 'probation_period_id') {
                            getValue("{{ url('probation-period') }}/"+val,function (data) {
                                if (data) {
                                    $('[name="'+key+'"]').append('<option value="'+val+'" selected="selected">'+data.name+'</option>').change();
                                }
                            });
                        }
                    });
                },
                complete:function (xhrs,status) {
                    if (status !=="success") {
                        swal({
                            title:"There have no setting for this Designation"
                        },function (isConfirm) {
                            if (isConfirm) {
                                setValueAsZero(['[name^="min_"]','[name^="max_"]'])
                            }
                        });
                    }
                }
            });
        }
        @if(!empty($id))
            setMinMaxValue({{ $id->designation_id }});
        @endif
//        End Set Min and Max Valur


//        SetGrossValueintoSalaryTYpe
        function SetGrossValueintoSalaryTYpe(value) {
            if (typeof value === 'undefined') {
                value = $('[name="gross_salary"]').val()
            }
            var medical = $('[name="medical"]').val();
            var food = $('[name="food_all"]').val();
            var conveyance =$('[name="conveyance"]').val();
            var basic = (parseFloat(value)-(parseFloat(medical)+parseFloat(food)+parseFloat(conveyance)))/1.40;
            var hra =basic/100*40;
            $('[name="basic_salary"]').val(basic).trigger('change');
            $('[name="hra"]').val(hra).trigger('change')
        }

        $('[name="gross_salary"]').on('change',function () {
            SetGrossValueintoSalaryTYpe($(this).val())
        });
        $('[name="medical"],[name="food_all"],[name="conveyance"]').on('change',function () {
            SetGrossValueintoSalaryTYpe()
        });



//        Set Zero value in field list
        function setValueAsZero(fields) {
            if (typeof fields === 'undefined') {
                return
            }
            $.each(fields,function (index,el) {
                $(el).val(0)
            });
        }


//        Set Confirm Date
        function SetConfirmDate() {
            var month = $('[name="probation_period_id"] option:selected').html()
            if (month) {
                month = month.split(' ')[0]
            }
            var date_of_joining = $('[name="date_of_joining"]').val();
            if (date_of_joining && month) {
                date_of_joining = new Date(date_of_joining);
                var date_of_confirm = new Date(date_of_joining.setMonth(date_of_joining.getMonth()+parseInt(month)));
                $('[name="confirm_date"]').val(date_of_confirm.getFullYear()+'-'+leftPad(date_of_confirm.getMonth(),2)+
                        '-'+leftPad(date_of_confirm.getDate(),2))
            }
        }

//        SetConfirmDate()
        $('[name="date_of_joining"],[name="probation_period_id"]').on('change',function () {
            SetConfirmDate()
        });


//         SetIncrimentDate
        function SetIncrimentDate() {
            var month = $('[name="increment_month"]').val();
            var last_increment_date = $('[name="last_increment_date"]').val();
            if (last_increment_date && month) {
                last_increment_date = new Date(last_increment_date);
                var increment_date = new Date(last_increment_date.setMonth(last_increment_date.getMonth()+parseInt(month)));
                $('[name="increment_date"]').val(increment_date.getFullYear()+'-'+leftPad(increment_date.getMonth(),2)+
                        '-'+leftPad(increment_date.getDate(),2))
            }
        }

        $('[name="increment_month"],[name="last_increment_date"]').on('change',function () {
            SetIncrimentDate()
        });


//        Joning Salary
        $('[name="joining_salary"]').on('change',function () {
            @if(empty($id))
                $('[name="current_salary"]').val($(this).val());
                $('[name="gross_salary"]').val($(this).val()).trigger('change');
            @endif
        });
    </script>
@endsection