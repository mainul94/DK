<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 6/14/16
 * Time: 12:14 AM
 */
$sl = 1;
if (!empty($_REQUEST['page'])) {
    $sl += ($_REQUEST['page']-1) *15;
}
?>


@extends('layouts.layout')


@section('content')
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="widget">
                <div class="widget-header ">
                    <span class="widget-caption">Salary Report Section Wise</span>
                    {{--<div class="widget-buttons">
                        <a href="#" data-toggle="maximize">
                            <i class="fa fa-expand"></i>
                        </a>
                        <a href="#" data-toggle="collapse">
                            <i class="fa fa-minus"></i>
                        </a>
                        <a href="#" data-toggle="dispose">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>--}}
                </div>
                <div class="widget-body">
                    <div class="col-xs-12">
                        {!! Form::open(['action'=>['ReportController@salaryDesignation'],'method'=>'GET']) !!}
                        <div class="form-group col-xs-4 {{ $errors->has('month') ? 'has-error':''}}">
                            <div class="col-sm-4 text-right">
                                {!! Form::label('month','Month',['class'=>'field-required']) !!}
                            </div>
                            <div class="col-sm-8">
                                {!! Form::select('month',$months,$month,['class'=>'form-control']) !!}
                                {!! $errors->first('month','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="form-group col-xs-3 {{ $errors->has('year') ? 'has-error':''}}">
                            <div class="col-sm-4 text-right">
                                {!! Form::label('year','Year',['class'=>'field-required']) !!}
                            </div>
                            <div class="col-sm-8">
                                {!! Form::select('year',$years,$year,['class'=>'form-control']) !!}
                                {!! $errors->first('year','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="col-xs-2">
                            <div class="col-xs-10 pull-right">
                                <button type="submit" class="btn btn-info">Filter</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="table-responsive reportForPrint col-xs-12" style="max-height: 500px;">
                        <table class="table table-striped table-bordered table-hover"  id="salaryReport">
                            <caption class="text-center">
                                <h2>DK Global Fashion Ware Limited</h2>
                                <p> Salary Wages Sheet for the Month of:  {{ array_key_exists($month,$months)?$months[$month]:'' }} - {{ array_key_exists($year,$years)?$years[$year]:'' }}</p>
                            </caption>
                            <thead>
                            <tr>
                                <th>Section</th>
                                <th>No of Emp</th>
                                <th>Gross Salary</th>
                                <th>Ab Deduction</th>
                                <th>OT Amount</th>
                                <th>Ex OT Amount</th>
                                <th>Total OT Amount</th>
                                <th>Attendance Bonus</th>
                                <th>Holiday All</th>
                                <th>Arrear</th>
                                <th>Festival Bonus</th>
                                <th>Incentive</th>
                                <th>Advance</th>
                                <th>Others Deduction</th>
                                <th>Tax Deb</th>
                                <th>Stamp Deb</th>
                                <th>Payable Total</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($rows as $row)
                                    <tr>
                                        <td>{!! $row['designation_name'] !!}</td>
                                        <td>{!! $row['no_of_emp'] !!}</td>
                                        <td>{!! $row['gross_salary'] !!}</td>
                                        <td>{!! $row['ab_deduction'] !!}</td>
                                        <td>{!! $row['ot_amount'] !!}</td>
                                        <td>{!! $row['ext_ot_amount'] !!}</td>
                                        <td>{!! $row['ot_amount'] + $row['ext_ot_amount'] !!}</td>
                                        <td>{!! $row['attendance_bonus'] !!}</td>
                                        <td>{!! $row['holiday_all'] !!}</td>
                                        <td>{!! $row['arrear'] !!}</td>
                                        <td>{!! $row['fest_bonus'] !!}</td>
                                        <td>{!! $row['incentive'] !!}</td>
                                        <td>{!! $row['advance'] !!}</td>
                                        <td>{!! $row['others_deduction'] !!}</td>
                                        <td>{!! $row['tax'] !!}</td>
                                        <td>{!! $row['revenue_stamp'] !!}</td>
                                        <td>{!! ($row['gross_salary']+$row['ot_amount']+$row['ext_ot_amount']+
                                        $row['attendance_bonus']+$row['arrear']+$row['fest_bonus']+$row['incentive'] )
                                        -($row['ab_deduction']+$row['advance']+$row['others_deduction']+$row['tax']+
                                        $row['revenue_stamp'] ) !!}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
{{--                    {!! $rows->render() !!}--}}
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('style_sheet')
    <style>
        #salaryReport hr {
            margin: 5px 0;
        }
        #salaryReport th {
            min-width: 60px;
        }
    </style>
@endsection

@section('footer_script')
    <script src="{{ url('assets/js/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('assets/js/datatable/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ url('assets/js/datatable/dataTables.bootstrap.min.js') }}"></script>
    <script>
        var InitiateSalaryReportTable = function() {
            return {
                init: function() {
                    var oTable = $('#salaryReport').dataTable({
                        "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                        // "aLengthMenu": [
                        //     [5, 15, 20, -1],
                        //     [5, 15, 20, "All"]
                        // ],
                        // "iDisplayLength": 10,
                        "oTableTools": {
                            "aButtons": [
                                "print"
                            ]
                        },
                        "language": {
                            "search": "",
                            "sLengthMenu": "_MENU_",
                            "oPaginate": {
                                "sPrevious": "Prev",
                                "sNext": "Next"
                            }
                        },
                        "searching": false,
                        "paging":   false,
                        "ordering": false,
                        "info":     false
                    });

                    /*$("tfoot input").keyup(function() {
                     /!* Filter on the column (the index) of this element *!/
                     oTable.fnFilter(this.value, $("tfoot input").index(this));
                     });*/
                    $('.DTTT_button_print').on('click',function (event) {
                        event.preventDefault();
                        window.print();
                    });
                }
            };
        }();
        InitiateSalaryReportTable.init();
    </script>
@endsection