<?php

namespace App;

use App\Model\CommonField;
use Illuminate\Database\Eloquent\Model;

class SalaryType extends Model
{
    protected $fillable = ['name','earning','deduction','created_by','updated_by'];

    use CommonField;

}
