<?php

namespace App;

//use Log;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class Import extends Excel
{
    public static function importAttendance($row,$user)
    {
//        $settings = Setting::lists('value','property');
        $d = $row->get('d_card');
        $t = $row->get('t_card');

        if (empty($d) || empty($t)) {
            return;
        }

        $date = Carbon::parse($d.' '.(strlen($t) == 5? 0:'').$t);
//        Check Employee Exists
        $employee = Employee::where('old_card_no',$row->get('card_no'))->first();

        if (empty($employee)) {
            return ['type'=>'danger','msg'=>'Employee Not found'];
        }

        $settings = Shift::find($employee->shift_id);

        if (empty($settings)) {
            return ['type'=>'danger', 'msg'=>'Employee shift is not defined'];
        }

//        Check Employee Leave
        $checkLeave = leaveRegister::where('employee_id',$employee->id)->where('p_start_date','<=',$date->toDateString())
            ->where('p_end_date','>=', $date->toDateString())
            ->first();
        if (!empty($checkLeave)) {
            return ['type'=>'warning','msg'=>'Employee ID:'.$employee->old_card_no.' on Leave'];
        }
//        Check Attendance Exists
        $attendance = Attendance::where('employee_id',$employee->id)
            ->where('date',$date->toDateString())->first();
//        $officeOpenTime = explode(':',$settings['office_opening_time']);
        $officeOpenTime = explode(':', $settings->office_opening_time);
        $officeCloseTime = explode(':', $settings->office_closing_time);

        $officeOpenDateTime = Carbon::create($date->year,$date->month,$date->day,$officeOpenTime[0],$officeOpenTime[1],$officeOpenTime[2]);
        $officeCloseDateTime = Carbon::create($date->year, $date->month, $date->day, $officeCloseTime[0], $officeCloseTime[1], $officeCloseTime[2]);

//        $allowTime = Carbon::parse($settings['attendance_buffer_time'])->minute;
        $allowTime = Carbon::parse($settings->attendance_buffer_time)->minute;
//        dd($officeOpenDateTime->diffInMinutes($date,false));

        $isNightShift = false;

        if ($officeCloseDateTime < $officeOpenDateTime) {
            $isNightShift = true;
        }

        if (empty($attendance)) {
            $attendance = new Attendance;

            $attendance->fill([
                'employee_id' => $employee->id,
                'date' => $date->toDateString(),
                'in_time' => $date->toTimeString(),
                'status' => 'Present',
                'created_by' => $user
            ]);

            if ($officeOpenDateTime->diffInMinutes($date,false) > $allowTime) {
                $attendance->fill([
                    'let_time' => Carbon::create($date->year,$date->month,$date->day,0,0,0)
                    ->addMinute($officeOpenDateTime->diffInMinutes($date,false) - $allowTime)->toTimeString()
                ]);
            } elseif ($officeOpenDateTime->diffInMinutes($date,false) < -15) {
                $attendance->fill([
                    'in_time'=>$officeOpenDateTime->subMinute(random_int(1, 15))
                ]);
            }
        } else {
            if (Carbon::parse($date->toDateString().$attendance->in_time)->getTimestamp() == $date->getTimestamp()) {
                return ['type'=>'info','msg'=>'Already Entry'];
            }

            $inDateTime = $date->copy();

            if ($isNightShift) {
                $date->addDay();
            }

            $attendance->fill([
                'out_time' => $date->toTimeString(),
                'updated_by' => $user
            ]);

            $intimeExplode = explode(':', $attendance->in_time);
//            $intime_minute = Carbon::create($date->year, $date->month, $date->day, $intimeExplode[0], $intimeExplode[1], $intimeExplode[2]);
            $intime_minute = Carbon::create($inDateTime->year, $inDateTime->month, $inDateTime->day, $intimeExplode[0], $intimeExplode[1], $intimeExplode[2]);

//            $launch_explode = explode('-', $settings['lunch_break']);
            $launch_explode = explode('-', $settings->lunch_break);
            $launch_start = Carbon::parse($date->toDateString().$launch_explode[0]);
            $launch_end = Carbon::parse($date->toDateString().$launch_explode[1]);
            $launch_in_minute = $launch_start->diffInMinutes($launch_end);

//            $min_working_hour_min = $settings['min_working_hours']*60;
            $min_working_hour_min = $settings->min_working_hours*60;
//            $office_duration_min = $settings['office_duration_time']*60;
            $office_duration_min = $settings->office_duration_time*60;

            $actual_workin_min = $intime_minute->diffInMinutes($date);
            $woking_duration = $actual_workin_min;

            if ($launch_end->diffInMinutes($date) >= 0) {
                $woking_duration -= $launch_in_minute;
            } else if ($launch_start->diffInMinutes($date) >= 0 && $launch_end->diffInMinutes($date) < 0) {
                $woking_duration -= $launch_start->diffInMinutes($date);
            }

//            $overTime = $actual_workin_min - (($office_duration_min > 0) ? ($actual_workin_min - $office_duration_min) : 0);
            $overTime = $actual_workin_min - $office_duration_min;
            $overTimeModule = $overTime%60;

            if ($overTimeModule >= 50) {
                $overTime += 60 - $overTimeModule;
            }

            if ($actual_workin_min < $min_working_hour_min) {
                $attendance->fill(['status'=>'Absent']);
            } else {
                $attendance->fill(['status'=>'Present']);
            }

            $dateZero = Carbon::create($date->year, $date->month, $date->day, 0, 0, 0);

            $attendance->fill([
                'overtime' => $dateZero->addMinute($overTime)->toTimeString(),
                'duration' => $dateZero->addMinute($woking_duration - $overTime)->toTimeString()
            ]);
        }

        if ($attendance->save()) {
            return ['type'=>'success', 'msg'=>'Success Fully Insert # Employee ID:'.$employee->old_card_no];
        } else {
            return ['type'=>'error', 'msg'=>'Sorry Unable to Insert in # Employee ID:'.$employee->old_card_no];
        }
    }
}
