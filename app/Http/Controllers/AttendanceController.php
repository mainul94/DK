<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Employee;
use App\EmployeeCategory;
use App\Line;
use Illuminate\Http\Request;

use App\Http\Requests;

class AttendanceController extends Controller
{
    //function for index()//
    public function index(Request $request)
    {
        $atten = Attendance::orderBy('date', 'desc');
        if ($request->get('employee_id')) {
            $rows = Employee::findOrFail($request->get('employee_id'));
            $employees = [$rows->id,$rows->card_no];
            $employee_id = $rows->id;
            $atten->where('employee_id',$employee_id);
        }
        if ($request->get('from_date')) {
            $atten->where('date','>=', $request->get('from_date'));
        }
        if ($request->get('to_date')) {
            $atten->where('date','<=', $request->get('to_date'));
        }
        $attendance = $atten->paginate(100);
        $overtimes = $atten->sum('overtime');
        return view ('attendance.listview',compact('attendance','employees','employee_id', 'from_date', 'to_date', 'overtimes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('attendance.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'employee_id'=>'required',
        ]);
        $values = $request->all();
        $values['created_by'] = $request->user()->id;
        Attendance::create($values);

        if ($request->ajax()) {
            return response()->json([
                'message'=>'Success'
            ]);
        }

        return redirect()->back()->with('message',['type'=>'success','msg'=>'Successfully Saved']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('attendance.edit',compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id->fill($request->all());
        $id->fill(['updated_by'=>$request->user()->id]);
        $id->save();
        return redirect()->to(action('AttendanceController@edit',$id))->with('message',['type'=>'success','msg'=>'Successfully Updated']);
    }


    /**
     * View Multi Attendance
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function multiAttendance(Request $request)
    {
        $lines = [];
        $line_id = null;
        $emp_categories = [];
        $emp_category= null;
        $date = date('Y-m-d');
        if (!empty($request->get('line'))) {
            $this->setDate = empty($request->get('date'))?$date:$request->get('date');
            $employees = Employee::where('employees.line_id',$request->get('line'))->leftJoin('attendances', function ($join) {
                $join->on('employees.id', '=', 'attendances.employee_id')
                    ->where('attendances.date','=', $this->setDate);
            })->select('*','employees.id as employee_id','attendances.id as attendance_id');
            $line_id = $request->get('line');
            $lines = Line::findorFail($line_id)->lists('name','id');
            if (!empty($request->get('employee_category'))) {
                $emp_category = $request->get('employee_category');
                $emp_categories = EmployeeCategory::findorFail($emp_category)->lists('name','id');
                $employees->where('employees.employee_category_id',$emp_category);
            }
            $employees = $employees->get();
        }
        else {
            $employees = [];
        }
        if (!empty($request->get('date'))) {
            $date = $request->get('date');
        }
        return view('attendance.multi_attendance',compact('employees','lines','line_id','emp_categories','emp_category','date'));
    }


    /**
     * Save Multi Attendance
     * @param Request $request
     * @return array
     */
    public function multiAttendanceSave(Request $request)
    {
        $msg =[];
        foreach ($request->get('rows') as $row) {
            $row['date'] = $request->get('date',date('Y-m-d'));
            if (!empty($row['attendance_id'])) {
                $attendance = Attendance::findOrFail($row['attendance_id']);
                $row['updated_by'] = auth()->user()->id;
                if ($attendance) {
                    $attendance->fill($row)->save();
                }
            } else {
                $row['created_by'] = auth()->user()->id;
                Attendance::create($row);
            }
        }
        return response()->json([
            'msg'=>"Successfully Saved"
        ]);
    }
}
