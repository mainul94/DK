<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 3/25/16
 * Time: 4:05 PM
 */
$sl = 1;
?>


@extends('layouts.layout')


@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="well with-header">
            <div class="header bg-info">
                <div class="row">
                    <div class="col-sm-7 col-xs-12">
                        Salary Type List
                    </div>
                </div>
            </div>

            <div class="col-xs-12 text-right padding-bottom-20px">
                {!! Form::open(['action'=>['SalaryController@index'],'method'=>'GET']) !!}
                <div class="form-group col-xs-4 {{ $errors->has('month') ? 'has-error':''}}">
                    <div class="col-sm-4 text-right">
                        {!! Form::label('month','Month',['class'=>'field-required']) !!}
                    </div>
                    <div class="col-sm-8">
                        {!! Form::select('month',$months,$month,['class'=>'form-control']) !!}
                        {!! $errors->first('month','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="form-group col-xs-3 {{ $errors->has('year') ? 'has-error':''}}">
                    <div class="col-sm-4 text-right">
                        {!! Form::label('year','Year',['class'=>'field-required']) !!}
                    </div>
                    <div class="col-sm-8">
                        {!! Form::select('year',$years,$year,['class'=>'form-control']) !!}
                        {!! $errors->first('year','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-xs-2">
                    <div class="col-xs-10 pull-right">
                        <button type="submit" class="btn btn-info">Filter</button>
                    </div>
                </div>
                {!! Form::close() !!}
                <a href="{!! action('SalaryController@create') !!}" class="btn btn-info">Create</a>
            </div>
            @if(!empty(session()->has('message')))
                <div class="col-xs-12">
                    <div class="alert alert-{{session('message')['type']}} fade in">
                        <button class="close" data-dismiss="alert">
                            ×
                        </button>
                        <i class="fa-fw fa fa-check"></i>
                        <strong>Success</strong> {!! session('message')['msg'] !!}
                    </div>
                </div>
            @endif
            @if(count($rows)>0)
            <table class="table table-hover table-bordered">
                <thead class="bordered-blue">
                <tr>
                    <th>Sl.No</th>
                    <th>Card No</th>
                    <th>Name</th>
                    <th>Total Payable Days</th>
                    <th>Total Payable</th>
                    <th>Total Absent</th>
                    <th>Created By</th>
                    <th>updated By</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                        <tr>
                            <td>{!! $sl !!} <?php $sl++; ?></td>
                            <td>{!! $row->employee->card_no !!}</td>
                            <td>{!! $row->employee->name !!}</td>
                            <td>{!! $row->payable_days !!}</td>
                            <td>{!! $row->payable_amount !!}</td>
                            <td>{!! $row->ab_days !!}</td>
                            <td>{!! $row->createdBy->name !!}</td>
                            <td>
                                @if($row->updateBy)
                                    {!! $row->updateBy->name !!}
                                @endif
                            </td>
                            <td class="text-center">
                                <a href="{!! action('SalaryController@edit',$row->id) !!}" ><i class="fa fa-pencil-square-o text-danger"></i></a>
{{--                                {!! delete_data('SalaryController@destroy',$row->id) !!}--}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            @else
                <h3 class="text-muted text-center">
                    No Data
                </h3>
            @endif
        </div>

    </div>
</div>
@endsection