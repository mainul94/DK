<?php

namespace App;

use App\Model\CommonField;
use Illuminate\Database\Eloquent\Model;

class Line extends Model
{
    protected $fillable = ['name','organization_id','unit_id','division_id','department_id','section_id','created_by','updated_by'];


    use CommonField;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function allDesignations()
    {
        return $this->hasMany('App\Designation');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function employee()
    {
        return $this->hasMany('App\Employee');
    }
}
