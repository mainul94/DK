<?php

namespace App\Http\Controllers;

use App\Employee;
use App\EmployeeLeaveAssign;
use App\leaveType;
use Illuminate\Http\Request;

use App\Http\Requests;

class LeaveAssignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rows = EmployeeLeaveAssign::all();
        return view('leave_assign.listview',compact('rows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('leave_assign.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'employee_id'=>'required',
            'year'=>'required'
        ]);
        $values = $request->all();
        $values['created_by'] = $request->user()->id;
        foreach ($request->get('leave_days') as $key=>$val) {
            if (empty($val)) {
                continue;
            }
            $values['leave_days'] = $val;
            $values['leave_type_id'] = $request->get('leave_types')[$key];
            $employee = Employee::find($values['employee_id']);
            $leaveType = leaveType::find($values['leave_type_id']);
            if ($leaveType->is_maternity && $employee->sex != "Female") {
                continue;
            }
            if ($leaveType->is_earn) {
                continue;
            }
            EmployeeLeaveAssign::create($values);
        }

        if ($request->ajax()) {
            return response()->json([
                'message'=>'Success'
            ]);
        }

        return redirect()->back()->with('message',['type'=>'success','msg'=>'Successfully Saved']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('leave_assign.edit',compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeLeaveAssign $id)
    {
        $id->fill($request->all());
        $id->fill(['updated_by'=>$request->user()->id]);
        $id->save();
        return redirect()->to(action('LeaveAssignController@edit',$id))->with('message',['type'=>'success','msg'=>'Successfully Updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id->delete();
        return redirect()->to(action('LeaveAssignController@index'))->with('message',['type'=>'danger','msg'=>'Successfully Deleted']);
    }


    /**
     * Get Multiple Leave Assign Information
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getMultiple(Request $request)
    {
        $employees = Employee::where('line_id',$request->get('line')) ->get();

        return view('leave_assign.multiple',compact('employees'));

    }


    /**
     * Save Muliple Employee Leave Assign
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postMultiple(Request $request)
    {

        foreach ($request->get('employees') as $employee) {
            if (empty($employee['check'])) {
                continue;
            }
            $values = ['employee_id'=> $employee['id'],'year'=>$request->get('year')];
            foreach ($request->get('leavesAssigns') as $leave_assign) {
                if (empty($leave_assign['leave_days'])) {
                    continue;
                }
                $values['leave_days'] = $leave_assign['leave_days'];
                $values['leave_type_id'] = $leave_assign['leave_types'];
                $employee = Employee::find($values['employee_id']);
                $leaveType = leaveType::find($values['leave_type_id']);
                if ($leaveType->is_maternity && $employee->sex != "Female") {
                    continue;
                }
                if ($leaveType->is_earn) {
                    continue;
                }
                $leaveAssign = EmployeeLeaveAssign::where([
                    ['leave_type_id','=',$values['leave_type_id']],
                    ['employee_id','=',$values['employee_id']],
                    ['year','=',$values['year']],
                ])->first();
                if (!empty($leaveAssign)) {
                    $values['updated_by'] = $request->user()->id;
                    $leaveAssign->fill($values)->save();
                }else{
                    $values['created_by'] = $request->user()->id;
                    EmployeeLeaveAssign::create($values);
                }
            }
        }

        return response()->json([
            'msg'=>"Successfully Saved"
        ]);
    }

}
