<?php

namespace App\Http\Controllers;

use App\Employee;
use App\EmployeeLeaveAssign;
use App\leaveDetail;
use App\leaveRegister;
use App\leaveType;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

class LeaveRegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->get('employee_id')) {
            $emp = Employee::findOrFail($request->get('employee_id'));
            $employees = [$emp->id,$emp->card_no];
            $employee_id = $emp->id;
            $rows = leaveRegister::where('employee_id',$employee_id)->paginate(15);
        }else {
            $rows = leaveRegister::paginate(15);
        }
        return view('leave_register.listview',compact('rows','employees','employee_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('leave_register.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'employee_id'=>'required',
            'p_start_date'=>'required',
            'p_end_date'=>'required'
        ]);
        $values = $request->all();
        $values['created_by'] = $request->user()->id;
        $leaveRegister = new  leaveRegister;
        $leaveRegister->fill($values)->save();
        $values['leave_register_id'] = $leaveRegister->id;
        foreach ($request->get('leave_type_id') as $key=>$val) {
            if (empty($val)) {
                continue;
            }
            $values['leave_type_id'] = $val;
            $values['start_date'] = $request->get('start_date')[$key];
            $values['end_date'] = $request->get('end_date')[$key];
            $values['days'] = $request->get('days')[$key];
            $values['payable'] = $request->get('payable')[$key];
            leaveDetail::create($values);
        }

        if ($request->ajax()) {
            return response()->json([
                'message'=>'Success'
            ]);
        }

        return redirect()->back()->with('message',['type'=>'success','msg'=>'Successfully Saved']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('leave_register.view',compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(leaveRegister $id)
    {
        if ($id->is_permanent_save =="Yes") {
            return redirect()->action('LeaveRegisterController@show',$id->id);
        }
        $args = collect(['employee_id'=>$id->employee_id,'start_date'=>$id->p_start_date]);
        $avalableLeaves = $this->avalableLeaveCalculate($args);
        $avalableaveType = $avalableLeaves->pluck('name','leave_type_id');
        return view('leave_register.edit',compact('id','avalableaveType','avalableLeaves'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, leaveRegister $id)
    {
        foreach ($id->details as $detail) {
            if (in_array($detail->id,$request->get('cid'))) {
                continue;
            } else {
                $detail->delete();
            }
        }
        $childs = [];
        foreach ($request->get('leave_type_id') as $key=>$val) {
            if (empty($val)) {
                continue;
            }
            $values = [];
            $values['leave_type_id'] = $val;
            $values['start_date'] = $request->get('start_date')[$key];
            $values['end_date'] = $request->get('end_date')[$key];
            $values['days'] = $request->get('days')[$key];
            $values['payable'] = $request->get('payable')[$key];
            $values['id'] = $request->get('cid')[$key];
            if (!empty($values['id'])) {
                $leave = leaveDetail::find($values['id']);
                $leave->fill(['updated_by'=>$request->user()->id]);
            }else {
                $leave = new leaveDetail;
                $leave->fill(['leave_register_id'=>$id->id]);
                $leave->fill(['created_by'=>$request->user()->id]);
            }
            $leave->fill($values);
            $leave->save();
            array_push($childs,$leave->id);
        }
        $id->fill($request->all());
        $id->fill(['updated_by'=>$request->user()->id]);
//        dd($request->all());
        $id->update();
        return redirect()->to(action('LeaveRegisterController@edit',$id))->with('message',['type'=>'success','msg'=>'Successfully Updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id->delete();
        return redirect()->to(action('LeaveRegisterController@index'))->with('message',['type'=>'danger','msg'=>'Successfully Deleted']);
    }


    /**
     * Get Employee Avalable Leave From Client side
     * @param Request $request
     * @return \Illuminate\Support\Collection|string
     */
    public function avalableLeave(Request $request)
    {
        return $this->avalableLeaveCalculate($request);
    }


    /**
     * Calculate Employee Avalable Leave
     * @param $request
     * @return \Illuminate\Support\Collection|string
     */
    public function avalableLeaveCalculate($request)
    {
        if (empty($request->get('employee_id')) || empty($request->get('start_date'))) {
            return "Please fill Employee and Start Date";
        }
        $leaveAssign = EmployeeLeaveAssign::where([
            ['year','=',date('Y',strtotime($request->get('start_date')))],
            ['employee_id','=',$request->get('employee_id')]
        ])->get();

        $earn_leave = leaveRegister::earnLeaveOpening($request->get('employee_id'));
        $avableLeave = [];
        foreach ($leaveAssign as $leave) {
            $tmp = leaveDetail::where([
                ['leave_details.leave_type_id','=',$leave->leave_type_id]
            ])
                ->whereYear('leave_details.start_date','=',$leave->year)
                ->leftJoin('leave_registers','leave_details.leave_register_id','=','leave_registers.id')
                ->where('leave_registers.employee_id','=',$leave->employee_id)
                ->where('leave_registers.status','=',"Approved")
                ->where('leave_registers.is_permanent_save','=',"Yes")
                ->sum('leave_details.days');

            $leave->leave_days = $leave->leave_days - $tmp;
            $leaveType = leaveType::find($leave->leave_type_id);
            $leave->name = $leaveType->name;
            $avableLeave+=$leave->toArray();
        }
        $leaveAssign->add($earn_leave);

        return collect($leaveAssign);
    }
}
