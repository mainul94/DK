<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EarnLeaveOpeningBalance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('earn_leave_opening_balance', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id',0,1)->unique();
            $table->date('posting_date')->nullable();
            $table->integer('balance_days');
            $table->integer('payment_days');
            $table->foreign('employee_id')->references('id')->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('earn_leave_opening_balance',function(Blueprint $table){
            $table->dropForeign(['employee_id']);
        });

        Schema::drop('earn_leave_opening_balance');
    }
}
