<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 3/25/16
 * Time: 4:52 PM
 */
?>

@extends('layouts.layout')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-bottom bordered-blue">
                <span class="widget-caption">Create Designation Group</span>
            </div>
            <div class="widget-body">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-1">
                        {!! Form::open(['action'=>['DesignationGroupController@store']]) !!}
                        @include('designation_group._form')
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


