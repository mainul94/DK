<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Designation;
use App\Employee;
use App\Report;
use App\Salary;
use App\Section;
use PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

class ReportController extends Controller
{

    /**
     * @param Request $request
     * @param int $comp
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function salary(Request $request,$comp=0)
    {
        $sections = [];
        $year = $request->get('year')?$request->get('year'):date('Y');
        $month = $request->get('month')?$request->get('month'):date('m');
        $rows = Salary::where([['year','=',$year],['month','=',$month]]);
        $section_id = null;
        if (!empty($request->get('section_id'))) {
            $sections = Section::where('id',$request->get('section_id'))->lists('name','id');
            $section_id = $request->get('section_id');
            $employees = Employee::where('section_id',$request->get('section_id'))->get();
        }
        if (!empty($employees)) {
            $rows->whereIn('employee_id', $employees->lists('id'));
        }
        $rows = $rows->get();
        $component = compact('rows','month','year','comp','section_id','sections');
        if ($request->get('view_file')) {
            $pdf = PDF::loadView($request->get('view_file'), $component);
            if ($comp) {
                return $pdf->setPaper('legal', 'landscape')->download();
            }else {
                return $pdf->download();
            }
        }
        return view($request->get('view_file','report.salary'),$component);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function salaryPayslip(Request $request)
    {
        $year = $request->get('year')?$request->get('year'):date('Y');
        $month = $request->get('month')?$request->get('month'):date('m');
        $rows = Salary::where([['year','=',$year],['month','=',$month]])->paginate(100);

        return view('report.payslip',compact('rows','month','year'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function salaryDesignation(Request $request)
    {
        $year = $request->get('year')?$request->get('year'):date('Y');
        $month = $request->get('month')?$request->get('month'):date('m');
        $fields = ['no_of_emp'=>0,'gross_salary'=>0,'ab_deduction'=>0,'ot_amount'=>0,'ext_ot_amount'=>0,
            'total_ot_amount'=>0,'attendance_bonus'=>0,'holiday_all'=>0,'arrear'=>0,'fest_bonus'=>0,
            'incentive'=>0,'advance'=>0,'others_deduction'=>0,'tax'=>0,'revenue_stamp'=>0,'payable_amount'=>0];

        $designations = Section::all();

        $rows = [];
        foreach ($designations as $designation) {
            $rows[$designation->id] = ['designation_id'=>$designation->id,
                'designation_name'=>$designation->name];
            $rows[$designation->id] = array_merge($rows[$designation->id], $fields);
            $employees = $designation->employee->lists('id');
            $salaries = Salary::whereIn('employee_id',$employees->toArray())
                ->where('year',$year)->where('month',$month)->get();
            foreach ($salaries as $salary){
                $rows[$designation->id]['no_of_emp'] += 1;
                foreach ($salary->toArray() as $key=>$val) {
                    if (array_key_exists($key, $rows[$designation->id])) {
                        $rows[$designation->id][$key] += $val;
                    }
                }
            }
        }
        return view('report.salary_designation_wise',compact('rows','month','year'));
    }

    /**
     * @param Request $request
     */
    public function attendance(Request $request)
    {
        $year = $request->get('year')?$request->get('year'):date('Y');
        $month = $request->get('month')?$request->get('month'):date('m');
        $daysInMonth = Carbon::create($year,$month)->daysInMonth;

        $attendances = Attendance::where('id','!=',null)->attendanceFiltersReport($month,$year,$request)->get();
        dd($attendances);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Return Section Waise Attendance Report
     */
    public function attendanceSummary(Request $request)
    {
        $rows = [];
        $sections = [];
        $designations = [];
        $date = Carbon::now()->toDateString();
        $section_id = null;
        $designation_id = null;
        if (!empty($request->get('section_id'))) {
            $sections = Section::where('id',$request->get('section_id'))->lists('name','id');
            $section_id = $request->get('section_id');
        }

        if (!empty($request->get('designation_id'))) {
            $designations = Designation::where('id',$request->get('designation_id'))->lists('name','id');
            $designation_id = $request->get('designation_id');
        }

        if (!empty($request->get('date'))) {
            $rows = Report::attendanceSummary($request->get('date'),$section_id,$designation_id);
            $date = $request->get('date');
        }

        return view('report.attendance_summary',compact('rows','date','sections','section_id','designations','designation_id'));
    }


    /**
     * @param Request $request
     * @param string $view
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dailyAttendance(Request $request, $maxOt=0 ,$view='daily_attendance')
    {
        if ($request->route()->getName() == 'absent-list') {
            $view = 'absent_list';
        }
        $rows = [];
        $employee = null;
        $sections = [];
        $employees = [];
        $date = Carbon::now()->toDateString();
        $section_id = null;
        $employee_id = null;
        if (!empty($request->get('section_id'))) {
            $sections = Section::where('id',$request->get('section_id'))->lists('name','id');
            $section_id = $request->get('section_id');
            $rows = Section::where('id',$section_id)->get();
        }
        else if (empty($request->get('employee_id')))
        {
            $rows = Section::all();
        }

        if (!empty($request->get('employee_id'))) {
            $employees = Employee::where('id',$request->get('employee_id'))->lists('name','id');
            $employee_id = $request->get('employee_id');
            $employee = Employee::findOrFail($employee_id);
        }

        if (!empty($request->get('date'))) {
            $date = $request->get('date');
        }

        return view('report.'.$view,compact('rows','date','sections','section_id','employee','employees','employee_id','maxOt'));
    }


    /**
     * @param Request $request
     * @param int $maxOt
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function jobCard(Request $request, $maxOt=0)
    {
        $year = $request->get('year')?$request->get('year'):date('Y');
        $month = Carbon::now()->month;
        $employees = [];
        $employee_id = null;
        if ($request->get('month')) {
            $month = $request->get('month');
        }

        if ($request->get('employee_id')) {
            if (!empty($request->get('employee_id'))) {
                $employee_id = $request->get('employee_id');
                $employees = Employee::where('id',$employee_id)->lists('name','id');
                $employee = Employee::findOrFail($employee_id);
            }
        }
        
        return view('report.job_card',compact('employees','employee','employee_id','month','maxOt','year'));
        
    }


    /**
     * @param Request $request
     * @param null $employee
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function idCard(Request $request)
    {
        if ($request->get('employee_id')) {
            $employee_id = $request->get('employee_id');
            $employees = Employee::where('id',$employee_id)->lists('name','id');
            $rows = Employee::where('id',$employee_id)->get();
        } else {
            $rows = Employee::all();
            $employees = [];
            $employee_id = null;
        }

        return view('report.id_card',compact('employees','rows','employee_id'));
    }

    public function employee(Request $request)
    {
        $filter_fields = [
            'organization_id','unit_id','division_id','department_id','section_id','line_in','designation_id','employee_type_id',
            'employee_category_id'
        ];

		$values = $request->all();

		$employees = Employee::where('name','!=','');

		foreach ($filter_fields as $field) {
			if ($request->get($field)) {
				if (gettype($request->get($field)) === "string" || gettype($request->get($field)) === "integer" ) {
					$employees->where($field,$request->get($field));
				}else if (typeOf($request->get($field)) === "array" ) {
					$employees->whereIn($field,$request->get($field));
				}
			}
		}

		$employees = $employees->paginate(25);

		return view('report.employee', compact('employees'));



    }

}
