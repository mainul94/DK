<?php

namespace App\Http\Controllers;

use App\SalaryType;
use Illuminate\Http\Request;

use App\Http\Requests;

class SalaryTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rows = SalaryType::all();
        return view('salary_type.listview',compact('rows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('salary_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required'
        ]);
        $values = $request->all();
        $values['created_by'] = $request->user()->id;
        SalaryType::create($values);

        if ($request->ajax()) {
            return response()->json([
                'message'=>'Success'
            ]);
        }

        return redirect()->back()->with('message',['type'=>'success','msg'=>'Successfully Saved']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('salary_type.edit',compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SalaryType $id)
    {
        $values = $request->all();
        if (empty($request->get('earning')) && $id->is_earn) {
            $values['earning'] = null;
        }
        if (empty($request->get('deduction')) && $id->is_maternity) {
            $values['deduction'] = null;
        }
        $id->fill($values);
        $id->fill(['updated_by'=>$request->user()->id]);
        $id->save();
        return redirect()->to(action('SalaryTypeController@edit',$id))->with('message',['type'=>'success','msg'=>'Successfully Updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id->delete();
        return redirect()->to(action('SalaryTypeController@index'))->with('message',['type'=>'danger','msg'=>'Successfully Deleted']);
    }
}
