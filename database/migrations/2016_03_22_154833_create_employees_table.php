<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('card_no');
            $table->integer('organization_id')->unsigned();
            $table->integer('unit_id')->unsigned();
            $table->integer('division_id')->unsigned();
            $table->integer('department_id')->unsigned();
            $table->integer('section_id')->unsigned();
            $table->integer('line_id')->unsigned();
            $table->integer('designation_group_id')->unsigned();
            $table->integer('designation_id')->unsigned();
            $table->integer('employee_category_id')->unsigned();
            $table->integer('employee_type_id')->unsigned();
            $table->integer('holiday_id')->unsigned();
            $table->integer('probation_period_id')->unsigned();
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('organization_id')->references('id')->on('organizations');
            $table->foreign('unit_id')->references('id')->on('units');
            $table->foreign('division_id')->references('id')->on('divisions');
            $table->foreign('department_id')->references('id')->on('departments');
            $table->foreign('section_id')->references('id')->on('sections');
            $table->foreign('line_id')->references('id')->on('lines');
            $table->foreign('designation_group_id')->references('id')->on('designation_groups');
            $table->foreign('designation_id')->references('id')->on('designations');
            $table->foreign('employee_category_id')->references('id')->on('employee_categories');
            $table->foreign('employee_type_id')->references('id')->on('employee_types');
            $table->foreign('holiday_id')->references('id')->on('holidays');
            $table->foreign('probation_period_id')->references('id')->on('probation_periods');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->dropForeign(['organization_id']);
            $table->dropForeign(['unit_id']);
            $table->dropForeign(['division_id']);
            $table->dropForeign(['department_id']);
            $table->dropForeign(['section_id']);
            $table->dropForeign(['line_id']);
            $table->dropForeign(['designation_group_id']);
            $table->dropForeign(['designation_id']);
            $table->dropForeign(['employee_category_id']);
            $table->dropForeign(['employee_type_id']);
            $table->dropForeign(['holiday_id']);
            $table->dropForeign(['probation_period_id']);
            $table->dropForeign(['created_by']);
            $table->dropForeign(['updated_by']);
        });

        Schema::drop('employees');
    }
}
