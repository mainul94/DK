<?php

namespace App;

use App\Model\CommonField;
use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    protected $fillable = ['name','address','organization_id','unit_id','created_by','updated_by'];


    use CommonField;


}
