<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 8/10/16
 * Time: 11:41 PM
 */
?>
<tr>
    <td>{!! $sl !!}</td>
    <td>{!! $attendance->employee->card_no !!}</td>
    <td>{!! $attendance->date !!}</td>
    <td>{!! $attendance->in_time !!}</td>
    <td>{!! $attendance->out_time !!}</td>
    <td>{!! $attendance->duration !!}</td>
    <td>{!! $attendance->let_time !!}</td>
    <td>{!! $attendance->overtime !!}</td>
    <td>
        <span class="label label-{{ $attendance->status=="Present"?'success':'danger' }}">
            {!! $attendance->status !!}
        </span>
    </td>
    <td>{!! $attendance->createdBy->name !!}</td>
    <td>
        @if($attendance->updateBy)
            {!! $attendance->updateBy->name !!}
        @endif
    </td>
    <td class="text-center">

        <a href="{!! action('AttendanceController@edit',$attendance->id) !!}" ><i class="fa fa-pencil-square-o text-danger"></i></a>
    </td>
</tr>
