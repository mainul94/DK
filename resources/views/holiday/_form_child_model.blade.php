<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 4/3/16
 * Time: 7:52 PM
 */
?>

<!-- Modal -->
<div class="modal fade" id="holidayChildTable_new_multiple_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        {!! Form::open(['action'=>['HolidayChildController@multipleInsert',$id->id], 'method'=>'POST']) !!}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Genarate Multiple Days</h4>
            </div>
            <div class="modal-body" style="overflow: hidden">
                <div class="form-group col-xs-12 {{ $errors->has('day') ? 'has-error':''}}">
                    <div class="col-sm-2 text-right">
                        {!! Form::label('day','Day',['class'=>'field-required']) !!}
                    </div>
                    <div class="col-sm-10">
                        {!! Form::select('day',['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']
                        ,null,['class'=>'form-control']) !!}
                        {!! $errors->first('day','<span class="help-block">:message</span>') !!}
                    </div>
                </div>


                <div class="form-group col-xs-12 {{ $errors->has('type') ? 'has-error':''}}">
                    <div class="col-sm-2 text-right">
                        {!! Form::label('type','Type',['class'=>'field-required']) !!}
                    </div>
                    <div class="col-sm-10">
                        {!! Form::select('type',[],null,['class'=>'form-control']) !!}
                        {!! $errors->first('type','<span class="help-block">:message</span>') !!}
                    </div>
                </div>


                <div class="form-group col-xs-12 {{ $errors->has('purpose') ? 'has-error':''}}">
                    <div class="col-sm-2 text-right">
                        {!! Form::label('purpose','Purpose',['class'=>'field-required']) !!}
                    </div>
                    <div class="col-sm-10">
                        {!! Form::text('purpose',null,['class'=>'form-control']) !!}
                        {!! $errors->first('purpose','<span class="help-block">:message</span>') !!}
                    </div>
                </div>


                <div class="form-group col-xs-12 {{ $errors->has('details') ? 'has-error':''}}">
                    <div class="col-sm-2 text-right">
                        {!! Form::label('details','Details') !!}
                    </div>
                    <div class="col-sm-10">
                        {!! Form::textarea('details',null,['class'=>'form-control']) !!}
                        {!! $errors->first('details','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>