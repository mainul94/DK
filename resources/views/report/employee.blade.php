<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 6/14/16
 * Time: 12:14 AM
 */
?>


@extends('layouts.layout')


@section('content')
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="widget">
                <div class="widget-header ">
                    <span class="widget-caption">Employee Report</span>
                </div>
                <div class="widget-body">
                    <div class="col-xs-12">
                        {!! Form::open(['action'=>['ReportController@employee'],'method'=>'GET']) !!}
                            @include('report._employee_filters')
                        {!! Form::close() !!}
                    </div>
                    <div class="table-responsive reportForPrint col-xs-12">
                        <table class="table table-striped table-bordered table-hover" id="employees">
                            <caption class="text-center">
                                <h2><b>DK Global Fashion Wear Limited</b></h2>
								<p>Mollah Super Market,Beron,Jamgora,Ashulia,Savar,Dhaka</p>
								<p><b>Section Wise Employee List</b></p>
                            </caption>
                            <thead>
                            <tr>
                                <th>Sl No</th>
                                <th>ID No</th>
                                <th>Name</th>
								<th>Designation</th>
								<th>Grade</th>
                                <th>Section</th>
                                <th>Line</th>
								<th>Basic Salary</th>
								<th>Gross Salary</th>
                            </tr>
                            </thead>
                            <tbody>
                                @php $employees? $start = ($employees->currentPage()*$employees->perPage())-$employees->perPage(): $start =0 @endphp
                                @foreach($employees as $sl=>$employee)
                                    <tr>
                                        <td>{!! $start + ++$sl !!}</td>
                                        <td>{!! $employee->old_card_no !!}</td>
                                        <td>{!! $employee->name !!}</td>
										<td>{!! $employee->designation->name or "" !!}</td>
										<td>{!! $employee->designation->grade or "" !!}</td>
                                        <td>{!! $employee->section->name or "" !!}</td>
                                        <td>{!! $employee->line->name or "" !!}</td>
										<td>{!! $employee->basic_salary or "" !!}</td>
										<td>{!! $employee->gross_salary or "" !!}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-xs-12">
                        <br>
                        @include('report._employee_pagination')
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('style_sheet')
    <style>
        #salaryReport hr {
            margin: 5px 0;
        }
        #salaryReport th {
            min-width: 60px;
        }
    </style>
@endsection

@section('footer_script')
    <script src="{{ url('assets/js/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('assets/js/datatable/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ url('assets/js/datatable/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ url('assets/js/panel.js') }}"></script>
    <script>
        var InitiateSalaryReportTable = function() {
            return {
                init: function() {
                    var oTable = $('#employees').dataTable({
                        "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                        // "aLengthMenu": [
                        //     [5, 15, 20, -1],
                        //     [5, 15, 20, "All"]
                        // ],
                        // "iDisplayLength": 10,
                        "oTableTools": {
                            "aButtons": [
                                "print"
                            ]
                        },
                        "language": {
                            "search": "",
                            "sLengthMenu": "_MENU_",
                            "oPaginate": {
                                "sPrevious": "Prev",
                                "sNext": "Next"
                            }
                        },
                        "searching": false,
                        "paging":   false,
                        "ordering": false,
                        "info":     false
                    });

                    /*$("tfoot input").keyup(function() {
                        /!* Filter on the column (the index) of this element *!/
                        oTable.fnFilter(this.value, $("tfoot input").index(this));
                    });*/
                    $('.DTTT_button_print').on('click',function (event) {
                        event.preventDefault();
                        window.print();
                    });
                }
            };
        }();
        InitiateSalaryReportTable.init();

       getvalueForSelect2("select[name='organization_id']",'organizations',['id','name'],[],'id','name');
//        getvalueForSelect2("select[name='unit_id']",'units',['id','name'],[['organization_id']],'id','name');
//        getvalueForSelect2("select[name='division_id']",'divisions',['id','name'],[['organization_id'],['unit_id']],'id','name');
//        getvalueForSelect2("select[name='department_id']",'departments',['id','name'],[['organization_id'],['unit_id'],['division_id']],'id','name');
        getvalueForSelect2("select[name='section_id']",'sections',['id','name'],[['organization_id']],'id','name');
        getvalueForSelect2("select[name='line_id']",'lines',['id','name'],[['section_id']],'id','name');
        getvalueForSelect2("select[name='employee_type_id']",'employee_types',['id','name'],[],'id','name');
        getvalueForSelect2("select[name='employee_category_id']",'employee_categories',['id','name'],[],'id','name');
        getvalueForSelect2("select[name='designation_id']",'designations',['id','name'],[['line_id']],'id','name');
        getvalueForSelect2("select[name='employee_id']",'employees',['id','old_card_no'],[['designation_id']],'id','old_card_no');


    </script>
@endsection