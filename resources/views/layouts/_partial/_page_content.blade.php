<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 3/25/16
 * Time: 3:52 PM
 */
?>

<!-- Page Content -->
<div class="page-content">
    @include('layouts._partial._breadcrumb')
    <!-- Page Body -->
    <div class="page-body">
        @yield('content')
    </div>
    <!-- /Page Body -->
</div>
<!-- /Page Content -->
