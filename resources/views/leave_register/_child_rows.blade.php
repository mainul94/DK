<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 4/27/16
 * Time: 8:55 AM
 */
?>
<div class="table-toolbar">
    <a id="leaveRegisterChild_new" href="javascript:void(0);" class="btn btn-default">Add New Row</a>
</div>
<table class="table table-striped table-hover table-bordered" id="leaveRegisterChild">
    <thead>
    <tr role="row">
        <th>Sl. No</th>
        <th>Leave Type</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Days</th>
        <th>Is Payable</th>
        <th>Remove</th>
    </tr>
    </thead>

    <tbody>
    @if(!empty($id) && $id->details)
        @foreach($id->details as $key=>$child)
            <tr>
                {!! Form::model($child) !!}
                <td>{!! $key+1 !!}</td>
                <td class="{{ $errors->has("leave_type_id[]") ? "has-error":''}}">
                    {{ Form::select("leave_type_id[]",$avalableaveType,null,["class"=>"form-control"]) }}
                    {{ Form::hidden("cid[]",$child->id,["class"=>"form-control"]) }}
                    {!! $errors->first("leave_type_id[]","<span class=\"help-block\">:message</span>") !!}
                </td>
                <td class="{{ $errors->has("start_date[]") ? "has-error":''}}">
                    {{ Form::date("start_date[]",null,["class"=>"form-control"]) }}
                    {!! $errors->first("start_date[]","<span class=\"help-block\">:message</span>") !!}
                </td>
                <td class="{{ $errors->has("end_date[]") ? "has-error":''}}">
                    {{ Form::date("end_date[]",null,["class"=>"form-control"]) }}
                    {!! $errors->first("end_date[]","<span class=\"help-block\">:message</span>") !!}
                </td>
                <td class="{{ $errors->has("days[]") ? "has-error":''}}">
                    {{ Form::number("days[]",null,["class"=>"form-control","readonly"=>"readonly"]) }}
                    {!! $errors->first("days[]","<span class=\"help-block\">:message</span>") !!}
                </td>
                <td class="{{ $errors->has("payable[]") ? "has-error":''}}">
                    {{ Form::select("payable[]",["Yes","No"],null,["class"=>"form-control"]) }}
                    {!! $errors->first("payable[]","<span class=\"help-block\">:message</span>") !!}
                </td>
                <td>
                    <button class="remove-row btn btn-danger" type="button"> <i class="fa fa-trash-o"></i></button>
                </td>
                {{--{!! Form::close() !!}--}}
            </tr>
        @endforeach
    @endif
    </tbody>
</table>


@section("footer_script")
    <script>
        getvalueForSelect2('select[name="employee_id"]',"employees",["id","name"],[],"id","name");
        getvalueForSelect2('select[name="leave_approval"]',"users",["id","email"],[],"id","email");


        if (typeof avalableLeaves === "undefined") {
            @if(!empty($avalableLeaves))
                avalableLeaves = {!! $avalableLeaves !!};
            @else
                avalableLeaves = [];
            @endif

            $('#leaveRegisterChild tbody tr').each(function () {
                var me = this;
                $(me).find('[name="start_date[]"],[name="end_date[]"]').on('change', function () {
                    setDays($(me).find('[name="start_date[]"]').val(),$(me).find('[name="end_date[]"]').val(),$(me).find('[name="days[]"]'))
                });

                $(me).find('.remove-row').on('click', function () {
                    swal({
                        title: "Did you want to Remove?",
                        type: "error",
                        showCancelButton: true,
                    }, function (inConfurm) {
                        if (inConfurm) {
                            $(me).remove();
                            setSerialNumberInTable("#leaveRegisterChild tbody");
                        }
                    })
                });

            });
        }
        $("#leaveRegisterChild_new").on("click", function () {
            var $row = $("#leaveRegisterChild tbody").append(genarateNewRow());

            $row.find('.remove-row').on('click', function () {
                var me = this;
                swal({
                    title: "Did you want to Remove?",
                    type: "error",
                    showCancelButton: true
                }, function (inConfurm) {
                    if (inConfurm) {
                        $(me).parents('tr').remove();
                        setSerialNumberInTable("#leaveRegisterChild tbody");
                    }
                })
            });
            setSerialNumberInTable("#leaveRegisterChild tbody");
        });



//        New Row
        function genarateNewRow() {
            var $row = $("<tr></tr>");
                $row.append('<td>{!! 1 !!}</td>\
                    <td class="{{ $errors->has("leave_type_id[]") ? "has-error":""}}">\
                    {{ Form::select("leave_type_id[]",[],null,["class"=>"form-control"]) }}\
                    {{ Form::hidden("cid[]",null,["class"=>"form-control"]) }}\
                    {!! $errors->first("leave_type_id[]","<span class=\"help-block\">:message</span>") !!}\
                    </td>\
                    <td class="{{ $errors->has("start_date[]") ? "has-error":""}}">\
                    {{ Form::date("start_date[]",null,["class"=>"form-control"]) }}\
                    {!! $errors->first("start_date[]","<span class=\"help-block\">:message</span>") !!}\
                    </td>\
                    <td class="{{ $errors->has("end_date[]") ? "has-error":""}}">\
                    {{ Form::date("end_date[]",null,["class"=>"form-control"]) }}\
                    {!! $errors->first("end_date[]","<span class=\"help-block\">:message</span>") !!}\
                    </td>\
                    <td class="{{ $errors->has("days[]") ? "has-error":""}}">\
                    {{ Form::number("days[]",null,["class"=>"form-control","readonly"=>"readonly"]) }}\
                    {!! $errors->first("days[]","<span class=\"help-block\">:message</span>") !!}\
                    </td>\
                    <td class="{{ $errors->has("payable[]") ? "has-error":""}}">\
                    {{ Form::select("payable[]",["Yes","No"],null,["class"=>"form-control"]) }}\
                    {!! $errors->first("payable[]","<span class=\"help-block\">:message</span>") !!}\
                    </td>\
                    <td>\
                    <button class="remove-row btn btn-danger" type="button"> <i class="fa fa-trash-o"></i></button>\
                    </td>');
            if (avalableLeaves) {
                $.each(avalableLeaves, function (i,val) {
                    $row.find('[name="leave_type_id[]"]').append('<option value="'+val['leave_type_id']+'">'+val['name']+'</option>')
                });
            }
            $row.find('[name="start_date[]"],[name="end_date[]"]').on('change', function () {
                setDays($row.find('[name="start_date[]"]').val(),$row.find('[name="end_date[]"]').val(),$row.find('[name="days[]"]'))
            });
            return $row;
        }


//        SerialGenarate
        function setSerialNumberInTable (selector) {
            $(selector + " tr:visible").each(function (i) {
                $(this).children("td:first-child").html(i+1);
            });
        }

        setSerialNumberInTable("#leaveRegisterChild tbody");


//        Set avalable Leave
        function avalableLeaveView() {
            $("#avalableLeaveView tbody").html('');
            $.each(avalableLeaves, function (i,v) {
                $("#avalableLeaveView tbody").append('<tr><td>'+v['name']+'</td>><td>'+v['leave_days']+'</td></tr>')
            });
        }

//        Set Select to search for child select field
        function setOptionForChild() {
            $('#leaveRegisterChild').find('[name="leave_type_id[]"]').each(function () {
                $(this).html('');
                $.each(avalableLeaves, function (i,val) {
                    $(this).append('<option value="'+val['leave_type_id']+'">'+val['name']+'</option>');
                });
            });

//                call view avaleable
//            avalableLeaveView()

        }


        $('[name="employee_id"], [name="p_start_date"],[name="p_end_date"]').on('change', function () {

            setDays($('[name="p_start_date"]').val(),$('[name="p_end_date"]').val(),$('[name="p_days"]'));
        });
        $('[name="employee_id"], [name="p_start_date"]').on('change', function () {
            var url = "{{ url('emp-leave-avalable') }}?start_date="+$('[name="p_start_date"]').val()+"&employee_id="+$('[name="employee_id"]').val();

            getValue(url, function (data) {
                avalableLeaves = data;
                $('#leaveRegisterChild').find('tbody').html("");
                if (typeof data == "object") {
                    avalableLeaveView()
                }
            });
        });

//        Set Days
        function setDays(start,end,setin) {
            var first = new Date(start);
            var second = new Date(end);
            var days = Math.round((second-first)/(1000*60*60*24));
            setin.val(days+1)
        }

        $('form').on('submit', function () {
            var child_days = 0;
            $.each($('#leaveRegisterChild').find('[name="days[]"]'), function () {
                child_days +=parseInt($(this).val());
            });

            if (parseInt($('[name="p_days"]').val()) != parseInt(child_days)) {
                swal({
                    title: "!!Days doesn't match with main and child",
                    type: "warning"
                });
                return false;
            }

        });


    </script>

@endsection