<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalarySettingChildrenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_setting_children', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('salary_setting_id',0,1);
            $table->integer('salary_type_id',0,1);
            $table->double('min_amount');
            $table->double('max_amount');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('salary_setting_id')->references('id')->on('salary_settings');
            $table->foreign('salary_type_id')->references('id')->on('salary_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salary_setting_children',function(Blueprint $table){
            $table->dropForeign(['salary_settings']);
            $table->dropForeign(['salary_types']);
        });


        Schema::drop('salary_setting_children');
    }
}
