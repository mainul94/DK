<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLeaveApprovalInLeaveRegisterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leave_registers', function (Blueprint $table) {
            $table->integer('leave_approval',0,1)->after('p_days');
            $table->enum('status',['Pending','Approved','Rejected'])->after('p_days')->default('Pending');
            $table->enum('is_permanent_save',['Yes','No'])->default('No')->after('p_days');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leave_registers', function (Blueprint $table) {
            $table->dropColumn('leave_approval');
            $table->dropColumn('status');
            $table->dropColumn('is_permanent_save');
        });
    }
}
