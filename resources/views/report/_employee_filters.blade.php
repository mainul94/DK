<?php
/**
 * Created by PhpStorm.
 * User: Raju
 * Date: 8/30/16
 * Time: 1:26 AM
 */
?>
<div class="form-group col-xs-4 {{ $errors->has('organization_id') ? 'has-error':''}}">
	<div class="col-sm-12">
		{!! Form::label('organization_id','Organization') !!}
	</div>
	<div class="col-sm-12">
		@inject('organization','App\Organization')
		@php
			$organizations = $organization->find(request('organization_id'));
			$organizations =  $organizations?$organizations->lists('name','id'):[];
			$organization_id = request('organization_id');
		@endphp
		{!! Form::select('organization_id',$organizations,$organization_id,['class'=>'form-control']) !!}
		{!! $errors->first('organization_id','<span class="help-block">:message</span>') !!}
	</div>
</div>
{{--<div class="form-group col-xs-4 {{ $errors->has('unit_id') ? 'has-error':''}}">
	<div class="col-sm-12">
		{!! Form::label('unit_id','Unit') !!}
	</div>
	<div class="col-sm-12">
		@inject('unit','App\Unit')
		@php
			$units = $unit->find(request('unit_id'));
			$units =  $units?$units->lists('name','id'):[];
			$unit_id = request('unit_id');
		@endphp
		{!! Form::select('unit_id',$units,$unit_id,['class'=>'form-control']) !!}
		{!! $errors->first('unit_id','<span class="help-block">:message</span>') !!}
	</div>
</div>--}}
{{--<div class="form-group col-xs-4 {{ $errors->has('division_id') ? 'has-error':''}}">
	<div class="col-sm-12">
		{!! Form::label('division_id','Division') !!}
	</div>
	<div class="col-sm-12">
		@inject('division','App\Division')
		@php
			$divisions = $division->find(request('division_id'));
			$divisions =  $divisions?$divisions->lists('name','id'):[];
			$division_id = request('division_id');
		@endphp
		{!! Form::select('division_id',$divisions,$division_id,['class'=>'form-control']) !!}
		{!! $errors->first('division_id','<span class="help-block">:message</span>') !!}
	</div>
</div>--}}
<div class="form-group col-xs-4 {{ $errors->has('section_id') ? 'has-error':''}}">
	<div class="col-sm-12">
		{!! Form::label('section_id','Section') !!}
	</div>
	<div class="col-sm-12">
		@inject('section','App\Section')
		@php
			$sections = $section->find(request('section_id'));
			$sections =  $sections?$sections->lists('name','id'):[];
			$section_id = request('section_id');
		@endphp
		{!! Form::select('section_id',$sections,$section_id,['class'=>'form-control']) !!}
		{!! $errors->first('section_id','<span class="help-block">:message</span>') !!}
	</div>
</div>
<div class="form-group col-xs-4 {{ $errors->has('line_id') ? 'has-error':''}}">
	<div class="col-sm-12">
		{!! Form::label('line_id','Line') !!}
	</div>
	<div class="col-sm-12">
		@inject('line','App\Line')
		@php
			$lines = $line->find(request('line_id'));
			$lines =  $lines?$lines->lists('name','id'):[];
			$line_id = request('line_id');
		@endphp
		{!! Form::select('line_id',$lines,$line_id,['class'=>'form-control']) !!}
		{!! $errors->first('line_id','<span class="help-block">:message</span>') !!}
	</div>
</div>
<div class="form-group col-xs-4 {{ $errors->has('designation_id') ? 'has-error':''}}">
	<div class="col-sm-12">
		{!! Form::label('designation_id','Designation') !!}
	</div>
	<div class="col-sm-12">
		@inject('designation','App\Designation')
		@php
			$designations = $designation->find(request('designation_id'));
			$designations =  $designations?$designations->lists('name','id'):[];
			$designation_id = request('designation_id');
		@endphp
		{!! Form::select('designation_id',$designations,$designation_id,['class'=>'form-control']) !!}
		{!! $errors->first('designation_id','<span class="help-block">:message</span>') !!}
	</div>
</div>
{{--<div class="form-group col-xs-4 {{ $errors->has('department_id') ? 'has-error':''}}">
	<div class="col-sm-12">
		{!! Form::label('department_id','Department') !!}
	</div>
	<div class="col-sm-12">
		@inject('department','App\Department')
		@php
			$departments = $department->find(request('department_id'));
			$departments =  $departments?$departments->lists('name','id'):[];
			$department_id = request('department_id');
		@endphp
		{!! Form::select('department_id',$departments,$department_id,['class'=>'form-control']) !!}
		{!! $errors->first('department_id','<span class="help-block">:message</span>') !!}
	</div>
</div>--}}
<div class="form-group col-xs-4 {{ $errors->has('employee_type_id') ? 'has-error':''}}">
	<div class="col-sm-12">
		{!! Form::label('employee_type_id','Employee Type') !!}
	</div>
	<div class="col-sm-12">
		@inject('employee_type','App\EmployeeType')
		@php
			$employee_types = $employee_type->find(request('employee_type_id'));
			$employee_types =  $employee_types?$employee_types->lists('name','id'):[];
			$employee_type_id = request('employee_type_id');
		@endphp
		{!! Form::select('employee_type_id',$employee_types,$employee_type_id,['class'=>'form-control']) !!}
		{!! $errors->first('employee_type_id','<span class="help-block">:message</span>') !!}
	</div>
</div>
<div class="form-group col-xs-4 {{ $errors->has('employee_category_id') ? 'has-error':''}}">
	<div class="col-sm-12">
		{!! Form::label('employee_category_id','Employee Category') !!}
	</div>
	<div class="col-sm-12">
		@inject('employee_category','App\EmployeeCategory')
		@php
			$employee_categories = $employee_category->find(request('employee_category_id'));
			$employee_categories =  $employee_categories?$employee_categories->lists('name','id'):[];
			$employee_category_id = request('employee_category_id');
		@endphp
		{!! Form::select('employee_category_id',$employee_categories,$employee_category_id,['class'=>'form-control']) !!}
		{!! $errors->first('employee_category_id','<span class="help-block">:message</span>') !!}
	</div>
</div>
<div class="form-group col-xs-4 {{ $errors->has('employee_id') ? 'has-error':''}}">
	<div class="col-sm-12">
		{!! Form::label('employee_id','Employee') !!}
	</div>
	<div class="col-sm-12">
		@inject('employee','App\Employee')
		@php
			$employee_ids = $employee->find(request('employee_id'));
			$employee_ids =  $employee_ids?$employee_ids->lists('old_card_no','id'):[];
			$employee_id = request('employee_id');
		@endphp
		{!! Form::select('employee_id',$employee_ids,$employee_id,['class'=>'form-control']) !!}
		{!! $errors->first('employee_id','<span class="help-block">:message</span>') !!}
	</div>
</div>
<div class="form-group col-xs-4 pull-right">
	<div class="col-sm-12">
		<a href="{!! action('ReportController@employee') !!}" class="btn btn-warning">Reset</a>
		{!! Form::submit('Filters',['class'=>'btn btn-primary']) !!}
	</div>
</div>