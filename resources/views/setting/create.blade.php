<?php
/**
 * Created by PhpStorm.
 * User: raju
 * Date: 3/27/16
 * Time: 3:35 PM
 */?>


@extends('layouts.layout')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="widget">
                <div class="widget-header bordered-bottom bordered-blue">
                    <span class="widget-caption">Create Setting</span>
                </div>
                <div class="widget-body">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                            {!! Form::open(['action'=>['SettingController@store']]) !!}
                            @include('setting._form')
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

