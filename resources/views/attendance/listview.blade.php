<?php
/**
 * Created by PhpStorm.
 * User: Raju
 * Date: 3/25/16
 * Time: 4:05 PM
 */
$sl = 1;
if (!empty(request()->get('page'))) {
    $sl += (request()->get('page')-1) *100;
}
?>
@extends('layouts.layout')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="well with-header">
                <div class="header bg-info">
                    <div class="row">
                        <div class="col-sm-7 col-xs-12">
                            Attendance List
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 padding-bottom-20px">
                    {!! Form::open(['action'=>'AttendanceController@index', 'method'=>'get']) !!}
                    <div class="form-group col-xs-4 {{ $errors->has('employee_id') ? 'has-error':''}}">
                        <div class="col-sm-4 text-right">
                            {!! Form::label('employee_id','Employee') !!}
                        </div>
                        <div class="col-sm-8">
                            {!! Form::select('employee_id',!empty($employees)?$employees:[],!empty($employee_id)?$employee_id:null,['class'=>'form-control']) !!}
                            {!! $errors->first('employee_id','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                    <div class="form-group col-xs-3 {{ $errors->has('from_date') ? 'has-error':''}}">
                        <div class="col-sm-2 text-right">
                            {!! Form::label('from_date','From') !!}
                        </div>
                        <div class="col-sm-10">
                            {!! Form::date('from_date',null,['class'=>'form-control']) !!}
                            {!! $errors->first('from_date','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                    <div class="form-group col-xs-3 {{ $errors->has('to_date') ? 'has-error':''}}">
                        <div class="col-sm-2 text-right">
                            {!! Form::label('to_date','To') !!}
                        </div>
                        <div class="col-sm-10">
                            {!! Form::date('to_date',null,['class'=>'form-control']) !!}
                            {!! $errors->first('to_date','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                    <div class="form-group col-xs-1">
                        {!! Form::submit('Filter', ['class' => 'btn btn-info']) !!}
                    </div>

                    @if(!empty($employee_id))
                        <div class="col-xs-4">
                            <a href="{{ action('AttendanceController@index') }}" class="btn btn-warning">Reset</a>
                        </div>
                    @endif
                    {!! Form::close() !!}
                    <a href="{!! action('AttendanceController@create') !!}" class="btn btn-info pull-right">Create</a>
                </div>
                @if(!empty(session()->has('message')))
                    <div class="col-xs-12">
                        <div class="alert alert-{{session('message')['type']}} fade in">
                            <button class="close" data-dismiss="alert">
                                ×
                            </button>
                            <i class="fa-fw fa fa-check"></i>
                            <strong>Success</strong> {!! session('message')['msg'] !!}
                        </div>
                    </div>
                @endif
                @if(count($attendance)>0)
                    <div class="col-xs-12 text-center"><strong>Total Overtime {!! $overtimes !!}</strong></div>
                    <table class="table table-hover table-bordered">
                        <thead class="bordered-blue">
                        <tr>
                            <th>Sl.No</th>
                            <th>Employee Id</th>
                            <th>Date</th>
                            <th>In Time</th>
                            <th>Out Time</th>
                            <th>Duration</th>
                            <th>Let Time</th>
                            <th>Overtime</th>
                            <th>Status</th>
                            <th>Created By</th>
                            <th>updated By</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!empty($attendance->id))
                            @include('attendance._row',['row'=>$attendance])
                        @else
                            @foreach($attendance as $attendance)
                                @include('attendance._row',['row'=>$attendance])
                                {{--*/ $sl++ /*--}}
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                @else
                    <h3 class="text-muted text-center">
                        No Data
                    </h3>
                @endif
            </div>
            <div>
                @if(empty($attendance->id))
                    {!! $attendance->render() !!}
                @endif
            </div>

        </div>
    </div>
@endsection
@section('footer_script')
    <script>
        getvalueForSelect2("select[name='employee_id']",'employees',['id','card_no'],[],'id','card_no');
        /*$("select[name='employee_id']").on('change',function () {
            $(this).parents('form').submit();
        });*/
    </script>
@endsection