<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 6/14/16
 * Time: 12:14 AM
 */
$sl = 1;
if (!empty($_REQUEST['page'])) {
    $sl += ($_REQUEST['page']-1) *15;
}
?>


@extends('layouts.layout')


@section('content')
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="widget">
                <div class="widget-header ">
                    <span class="widget-caption">Salry Report</span>
                    {{--<div class="widget-buttons">
                        <a href="#" data-toggle="maximize">
                            <i class="fa fa-expand"></i>
                        </a>
                        <a href="#" data-toggle="collapse">
                            <i class="fa fa-minus"></i>
                        </a>
                        <a href="#" data-toggle="dispose">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>--}}
                </div>
                <div class="widget-body">
                    <div class="col-xs-12">
                        {!! Form::open(['action'=>['ReportController@salaryPayslip'],'method'=>'GET']) !!}
                        <div class="form-group col-xs-4 {{ $errors->has('month') ? 'has-error':''}}">
                            <div class="col-sm-4 text-right">
                                {!! Form::label('month','Month',['class'=>'field-required']) !!}
                            </div>
                            <div class="col-sm-8">
                                {!! Form::select('month',$months,$month,['class'=>'form-control']) !!}
                                {!! $errors->first('month','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="form-group col-xs-3 {{ $errors->has('year') ? 'has-error':''}}">
                            <div class="col-sm-4 text-right">
                                {!! Form::label('year','Year',['class'=>'field-required']) !!}
                            </div>
                            <div class="col-sm-8">
                                {!! Form::select('year',$years,$year,['class'=>'form-control']) !!}
                                {!! $errors->first('year','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="col-xs-2">
                            <div class="col-xs-10 pull-right">
                                <button type="submit" class="btn btn-info">Filter</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="col-xs-12" id="paySlip">
                        <a href="#" class="print" rel="paySlip">Print</a>
                            @for($i = 1; $i < 14; $i++)
                        @foreach($rows as $key=>$row)
                            <div class="col-xs-6">
                                <header>
                                    <p>পে স্লিপ</p>
                                    <h2 class="text-center">DK Global Fashion Ware Limited</h2>
                                    <p class="text-center">Molla Super Market,Beron,Jamgora,Ashulia,Saver,Dhaka</p>
                                    <p><strong>মাসের নাম </strong> {{ \Carbon\Carbon::create($year,$month)->firstOfMonth()->format('d-M-Y') }} TO
                                    {{ \Carbon\Carbon::create($year,$month)->lastOfMonth()->format('d-M-Y') }}</p>
                                </header>
                                <article class="row">
                                    <div class="col-xs-6">
                                        <div class="row">
                                            <div class="col-xs-4">কার্ড  নং </div>
                                            <div class="col-xs-8">{!! digitConvertToBangla($row->employee->card_no) !!}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4">নাম  </div>
                                            <div class="col-xs-8">{!! $row->employee->name !!}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4">পদবী   </div>
                                            <div class="col-xs-8">{!! $row->employee->designation->name !!}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8">মোট কার্য দিবস সংখ্যা</div>
                                            <div class="col-xs-4">{!! digitConvertToBangla($row->total_days) !!}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8">মজুরী যোগ্য দিবস </div>
                                            <div class="col-xs-4">{!! digitConvertToBangla($row->payable_days) !!}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8">হাজিরা দিবস </div>
                                            <div class="col-xs-4">{!! digitConvertToBangla($row->p_days) !!}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8">অনুপস্থিতি  </div>
                                            <div class="col-xs-4">{!! digitConvertToBangla($row->ab_days) !!}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 text-center"><strong>প্রাপ্য মজুরী</strong></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8">নির্ধারিত মোট মজুরী </div>
                                            <div class="col-xs-4">{!! digitConvertToBangla($row->gross_salary) !!}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8">বাড়ী ভাড়া ভাতা </div>
                                            <div class="col-xs-4">{!! digitConvertToBangla($row->hra) !!}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8">খাদ্য ভাতা </div>
                                            <div class="col-xs-4">{!! digitConvertToBangla($row->food_all) !!}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8">যাতায়াত </div>
                                            <div class="col-xs-4">{!! digitConvertToBangla($row->conveyance) !!}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8">হাজীরা বোনাস </div>
                                            <div class="col-xs-4">{!! digitConvertToBangla($row->attendance_bonus) !!}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8">অতিরিক্ত কাজের টাকা </div>
                                            <div class="col-xs-4">{!! digitConvertToBangla($row->ot_amount) !!}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8"><strong>মোট প্রদেয় টাকা </strong></div>
                                            <div class="col-xs-4"><strong> = {!! digitConvertToBangla($row->payable_amount - $row->ext_ot_amount) !!}</strong></div>
                                        </div>
                                        <br>
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-12 text-center">কর্তৃপক্ষ </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="row">
                                            <div class="col-xs-4">সেকশন </div>
                                            <div class="col-xs-8">{!! $row->employee->section->name!!}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4">গ্রেড </div>
                                            <div class="col-xs-8">{!! $row->employee->designation->grade!!}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4">যোগদান </div>
                                            <div class="col-xs-8">{!! date('d-M-Y',strtotime($row->employee->date_of_joining)) !!}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8">অসুস্থতাজনিত ছুটি </div>
                                            <div class="col-xs-4">{!! digitConvertToBangla(0) !!}</div> {{-- TODo need to come from calculate --}}
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8">নৈমিত্তিক / অর্ঝিত ছুটি </div>
                                            <div class="col-xs-4">{!! digitConvertToBangla(0) !!}</div> {{-- TODo need to come from Earn Leave calculate --}}
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8">অতিঃ কাজের ঘন্টা </div>
                                            <div class="col-xs-4">{!! digitConvertToBangla($row->ot_hours) !!}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8">অতিঃ কাজের হার </div>
                                            <div class="col-xs-4">{!! digitConvertToBangla(round($row->ot_rate,2)) !!}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 text-center"><strong>কর্তনকৃত মজুরী </strong></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8">অনুপস্থিতি জনিত কর্তন </div>
                                            <div class="col-xs-4">{!! digitConvertToBangla($row->ab_deduction) !!}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8">অগ্রীম মজুরী বাবদ </div>
                                            <div class="col-xs-4">{!! $row->advance !!}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8">রেভীনু্  স্ট্যাম্প বাবদ </div>
                                            <div class="col-xs-4">{!! digitConvertToBangla($row->revenue_stamp) !!}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8"><strong>মোট কর্তন </strong></div>
                                            <div class="col-xs-4"><strong> = {!! digitConvertToBangla($row->ab_deduction + $row->advance + $row->revenue_stamp) !!}</strong></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 text-center">মজুরী  রশিদ বুঝিয়া পাইলাম</div>
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-12 text-center">প্রাপকের সহি </div>
                                        </div>
                                    </div>
                                </article>

                            </div>
                            {{--@if($key%4)
                                </div><div class="page-break">
                            @endif--}}
                        @endforeach

                            @if($i%4 == 0)
                        <div class="page-break"></div>
                            @endif
                            @endfor
                    </div>
                    {!! $rows->render() !!}
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('style_sheet')
    <style>
        #salaryReport hr {
            margin: 5px 0;
        }
        #salaryReport th {
            min-width: 60px;
        }
    </style>
@endsection
@section('footer_script')
    <script src="{{ asset('assets/js/jquery.printElement.js') }}"></script>
    <script>
        $(function() {

            $('#payslip').printElement();

        });
    </script>
@endsection