<?php

namespace App;

use App\Model\CommonField;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    protected $fillable = ['year','month','employee_id','gross_salary','basic_salary','hra','medical','food_all','conveyance',
        'mobile_bill','special_bill','car_bill','others','incentive','attendance_bonus','ab_deduction','ot_rate','ot_hours',
        'ot_amount','holiday_all','arrear','fest_bonus','advance','others_deduction','tax','revenue_stamp','payable_amount',
        'ext_ot_hours','ext_ot_amount','total_days','p_days','ab_days','payable_days','created_by','updated_by',
        'total_leave', 'payable_leave', 'without_pay_leave'];

    use CommonField;

    protected $firstDayMonth;
    protected $lastDayMonth;

    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }


    public function multipleSalaryGenarate($request)
    {
//        ToDo Friday allowance and Total Leaves
        $messages = [];
        $month = $request->get('month');
        $year = $request->get('year');
        $date = Carbon::createFromDate($year,$month);
        $firstDayOfMonth = $date->startOfMonth()->toDateString();// For Format ->toDateString()
        $lastDayOfMonth = $date->endOfMonth()->toDateString();
        $totalDayOfMonth = $date->daysInMonth;
        $this->firstDayMonth = $firstDayOfMonth;
        $this->lastDayMonth = $lastDayOfMonth;

        $employees = Employee::where('status','Active')->get();
        foreach ($employees as $employee) {
            $saspection_date = Carbon::parse($this->firstDayMonth)->diffInDays(Carbon::parse($employee->date_of_separation), false);
            $joning_date = Carbon::parse($this->lastDayMonth)->diffInDays(Carbon::parse($employee->date_of_joining), false);
            if (($employee->date_of_separation !="0000-00-00" && $saspection_date <= 0) || $joning_date > 0) {
                continue;
            }

//            Check Already Salary Created
            if ($this->checkSalaryCreated($employee->id, $month, $year)) {
                $msg = ['type'=>'info','msg'=>'Card No "'.$employee->card_no.'" Already Genareted'];
                array_push($messages, $msg);
                continue;
            }

            $salarySetting = SalarySetting::where([['designation_id','=',$employee->designation_id],
                ['employee_type_id','=',$employee->employee_type_id]])->first();
//            Check Salary Settings Created
            if (empty($salarySetting)) {
                $msg = ['type'=>'warning','msg'=>'Sorry! Unable to Create Salary for Card No "'.
                    $employee->card_no.'" Because No Salary Setting for Designatin "'.$employee->designation->name.
                '" and Employee Type "'.$employee->employeeType->name.'". This Employee Belong to this Certiancials'];
                array_push($messages, $msg);
                continue;
            }

            $totalHolidays = HolidayChild::where('holiday_id',$employee->holiday->id)
                ->whereBetween('date',[$firstDayOfMonth,$lastDayOfMonth])->count();

            $leaves = $this->leaveDays($employee->id, $firstDayOfMonth, $lastDayOfMonth);

//            Attendance and Overtime
            $attendance = $this->attendancesCalculate($employee->id, $month, $year, $salarySetting->ot_status);


            $salary = new Salary;

            $values = [];
            $values = $this->valueReArrange($employee,$values,$totalHolidays,$leaves,$attendance,$totalDayOfMonth,$salarySetting);
            $values['created_by'] = $request->user()->id;
            $values['month'] = $month;
            $values['year'] = $year;
            $salary->fill($values);

            try {
                $salary->save();
                $msg = ['type'=>'success','msg'=>'Card No "'.$employee->card_no.'" Successfully Genareted'];
                array_push($messages, $msg);
            } catch (Exception $e) {
                $msg = ['type'=>'danger','msg'=>'Sorry! Unable to Card No Genareted for Card No: "'.$employee->card_no.
                    '" for contact to Support. <br><pre>'.$e.'</pre>'];
                array_push($messages, $msg);
            }
        }

        return $messages;
    }


    public function checkSalaryCreated($employee_id,$month,$year)
    {
        return Salary::where([['employee_id','=',$employee_id],['month','=',$month],['year','=',$year]])->count();
    }


//    Get Toatal Leave
    public function leaveDays($employee_id,$start,$end)
    {
        $totaPaytableInMonth = leaveRegister::whereDate('p_start_date','<=',date('Y-m-d',strtotime($end)))
            ->whereDate('p_end_date','>=',date('Y-m-d',strtotime($start)))->where('employee_id',$employee_id)->get();

        $payable = 0;
        $notPayable = 0;

        $earnType = leaveType::where('is_earn',1)->first();

        foreach ($totaPaytableInMonth as $item) {
            if (empty($item->details)) {
                continue;
            }
            foreach ($item->details as $child) {
                if ($child->leave_type_id == $earnType->id) {
                    continue;
                }
                if (date('Y-m-d', $child->p_start_date) >= date('Y-m-d', strtotime($start))
                    && date('Y-m-d', $child->p_end_date) <= date('Y-m-d', strtotime($end))) {

                    if ($child->payable == "Yes") {
                        $payable += $child->days;
                    } else {
                        $notPayable += $child->days;
                    }

                }else if (date('Y-m-d', $child->p_start_date) < date('Y-m-d', strtotime($start))
                    && date('Y-m-d', $child->p_end_date) >= date('Y-m-d', strtotime($start))) {
                    $stDate = Carbon::createFromFormat('yyyy-mm-dd', $start);
                    $endDate = Carbon::createFromFormat('yyyy-mm-dd', $child->p_end_date);
                    $days = $stDate->diffInDays($endDate);
                    if ($child->payable == "Yes") {
                        $payable += $days;
                    } else {
                        $notPayable += $days;
                    }
                }else if (date('Y-m-d', $child->p_start_date) <= date('Y-m-d', strtotime($end))
                    && date('Y-m-d', $child->p_end_sdate) > date('Y-m-d', strtotime($end))) {
                    $stDate = Carbon::createFromFormat('yyyy-mm-dd', $child->p_start_date);
                    $endDate = Carbon::createFromFormat('yyyy-mm-dd', $end);
                    $days = $stDate->diffInDays($endDate);
                    if ($child->payable == "Yes") {
                        $payable += $days;
                    } else {
                        $notPayable += $days;
                    }
                }else if (date('Y-m-d', $child->p_start_date) < date('Y-m-d', strtotime($start))
                    && date('Y-m-d', $child->p_end_date) > date('Y-m-d', strtotime($end))) {
                    $stDate = Carbon::createFromFormat('yyyy-mm-dd', $start);
                    $endDate = Carbon::createFromFormat('yyyy-mm-dd', $end);
                    $days = $stDate->diffInDays($endDate);
                    if ($child->payable == "Yes") {
                        $payable += $days;
                    } else {
                        $notPayable += $days;
                    }
                }
            }
        }
        return collect([
            'total'=>$payable+$notPayable,
            'payable'=>$payable,
            'notPayable'=>$notPayable
        ]);
    }


    public function attendance($employee_id,$date)
    {
        return Attendance::where('employee_id',$employee_id)->whereMonth('date','=',$date->month)
            ->whereYear('date','=',$date->year);
    }

//    Get Attendance and Overtime
    public function attendancesCalculate($employee_id,$month,$year,$ot_status="No")
    {
        $date = Carbon::createFromDate($year,$month);

        $totalPresent = $this->attendance($employee_id,$date);
        $totalPresent = $totalPresent->where('status','Present')->count();
        $totalAbsent = $this->attendance($employee_id,$date);
        $totalAbsent = $totalAbsent->where('status','Absent')->count();
        $totalOtHours = 0;
        $totalExtOtHours =0;
        if ($ot_status == "Yes"){
            $totalOts = $this->attendance($employee_id,$date);
            $totalOts = $totalOts->where('status','Present')->get();
            foreach ($totalOts as $totalOt) {
                    $time = explode(':', $totalOt->overtime);
                    $hour = Carbon::createFromTime($time[0],$time[1],$time[2]);

                    $hour->hour;
                if ($hour->hour > 2) {
                    $totalOtHours +=2;
                    $totalExtOtHours += $hour->hour -2;
                } else {
                    $totalOtHours += $hour->hour;
                }
            }
        }

        return collect([
            'present'=>$totalPresent,
            'absent'=>$totalAbsent,
            'total'=>$totalPresent+$totalAbsent,
            'otHours'=>$totalOtHours,
            'extOtHours'=>$totalExtOtHours
        ]);
    }


    public function valueReArrange($employee, $values, $totalHolidays, $leaves, $attendance,$totalDayOfMonth,$salarySetting)
    {
        $deduction_days = 0;
        $holi_last_date = $this->lastDayMonth;
        $holi_first_date = $this->firstDayMonth;
        $sep_date = Carbon::parse($this->firstDayMonth)->diffInDays(Carbon::parse($employee->date_of_separation));
        if ($sep_date > 0 && $sep_date <= $totalDayOfMonth) {
            $holi_last_date = $employee->date_of_separation;
            $deduction_days += $totalDayOfMonth - $sep_date;
        }

        $doj_date = Carbon::parse($this->lastDayMonth)->diffInDays(Carbon::parse($employee->date_of_joining));
        if ($doj_date > 0 && $doj_date <= $totalDayOfMonth) {
            $holi_first_date = $employee->date_of_joining;
            $deduction_days += $totalDayOfMonth - $doj_date;
        }
        $gross = $deduction_days? ($employee->gross_salary/$totalDayOfMonth)* ($totalDayOfMonth- $deduction_days) : $employee->gross_salary;

        $totalHolidays = HolidayChild::where('holiday_id',$employee->holiday->id)
                ->whereBetween('date',[$holi_first_date,$holi_last_date])->count();

        $values['employee_id'] = $employee->id;
        $values['gross_salary'] = $gross;
        $values['basic_salary'] = $employee->basic_salary;
        $values['hra'] = $employee->hra;
        $values['medical'] = $employee->medical;
        $values['food_all'] = $employee->food_all;
        $values['conveyance'] = $employee->conveyance;
        $values['mobile_bill'] = $employee->mobile_bill;
        $values['special_bill'] = $employee->special_bill;
        $values['car_bill'] = $employee->car_bill;
        $values['others'] = $employee->others;
        $values['incentive'] = $employee->incentive;
        $values['p_days'] = $attendance['present'];
        $values['ab_days'] = $attendance['absent'];
        $values['ot_hours'] = $attendance['otHours'];
        $values['ext_ot_hours'] = $attendance['extOtHours'];
        if ($attendance['absent'] || $leaves['total']) {
            $values['attendance_bonus'] = 0;
        } else if ($salarySetting->attendance_bonus) {
            $values['attendance_bonus'] = $salarySetting->attendance_bonus;
        }else {
            $values['attendance_bonus'] = 0;
        }
        if ($employee->employeeType->name=="Regular") {
            $values['ab_deduction'] = $employee->basic_salary/$totalDayOfMonth*($attendance['absent']+$leaves['notPayable']);
        } else {
            $values['ab_deduction'] = $employee->gross_salary/$totalDayOfMonth*($attendance['absent']+$leaves['notPayable']);
        }
        $values['ot_rate'] = $employee->basic_salary/104;
        $values['ot_amount'] = $values['ot_rate']*$attendance['otHours'];
        $values['ext_ot_amount'] = $values['ot_rate']*$attendance['extOtHours'];
        $values['holiday_all'] = $totalHolidays;
//        Deductions fields
        $values['advance'] = array_key_exists('advance', $values)? $values['advance']: 0;
        $values['others_deduction'] = array_key_exists('others_deduction', $values)? $values['others_deduction']: 00;
        $values['tax']= array_key_exists('tax', $values)? $values['tax']: 0;
        $values['revenue_stamp'] = array_key_exists('revenue_stamp', $values)? $values['revenue_stamp']: 10;

        $plusFields = ['gross_salary','mobile_bill','special_bill','car_bill','others','incentive','ot_amount','ext_ot_amount'];
        $minusField = ['ab_deduction','advance','others_deduction','tax','revenue_stamp'];
        $totalPayableAmount = 0;
        $totalMinusAmount = 0;
        foreach ($plusFields as $field) {
            $totalPayableAmount += $values[$field];
        }
        foreach ($minusField as $field) {
            $totalMinusAmount += $values[$field];
        }
        $values['payable_amount'] = $totalPayableAmount - $totalMinusAmount;
        $values['total_days'] = $totalDayOfMonth;
        $values['payable_days'] = $totalDayOfMonth - ($totalHolidays + $deduction_days);
        $values['total_leave'] = $leaves['total'];
        $values['payable_leave'] = $leaves['payable'];
        $values['without_pay_leave'] = $leaves['notPayable'];

        return $values;

    }

}
