<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 3/25/16
 * Time: 3:21 PM
 */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>@yield('title')</title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="shortcut icon" href="{!! asset('assets/img/favicon.png') !!}" type="image/x-icon">


    <!--Basic Styles-->
    <link href="{!! asset('assets/css/bootstrap.min.css') !!}" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="#" rel="stylesheet" />
    <link href="{!! asset('assets/css/font-awesome.min.css') !!}" rel="stylesheet" />
    <link href="{!! asset('assets/css/weather-icons.min.css') !!}" rel="stylesheet" />

    <!--Fonts-->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    <!--Beyond styles-->
    <link id="beyond-link" href="{!! asset('assets/css/beyond.min.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! asset('assets/css/demo.min.css') !!}" rel="stylesheet" />
    <link href="{!! asset('assets/css/typicons.min.css') !!}" rel="stylesheet" />
    <link href="{!! asset('assets/css/animate.min.css') !!}" rel="stylesheet" />
    <link id="skin-link" href="#" rel="stylesheet" type="text/css" />
    <link href="{!! asset('assets/sweetalert/sweetalert.css') !!}" rel="stylesheet" />
{{--    <link href="{!! asset('assets/css/select2.min.css') !!}" rel="stylesheet" />--}}
    <link href="{!! asset('assets/css/customize.css') !!}" rel="stylesheet" />

    @yield('style_sheet')
    <!--Skin Script: Place this script in head to load scripts for skins and rtl support-->
    <script src="{!! asset('assets/js/skins.min.js') !!}"></script>
    @yield('head_script')
</head>
<body>
<?php
$sparkline_chart=0;
$easy_pie_chart=0;
$flot_chart =0;
?>
    <!-- Loading Container -->

    <!--  /Loading Container -->
    @include('layouts._partial.navbar')

    <!-- Main Container -->
    <div class="main-container container-fluid">
        @include('layouts._partial.sidebar')
        @include('layouts._partial._chart_bar')
        @include('layouts._partial._page_content')
    </div>
    <!-- /Main Container -->


<!--Basic Scripts-->
<script src="{!! asset('assets/js/jquery.min.js') !!}"></script>
<script src="{!! asset('assets/js/bootstrap.min.js') !!}"></script>
<script src="{!! asset('assets/js/slimscroll/jquery.slimscroll.min.js') !!}"></script>
<script src="{!! asset('assets/js/attendance.js') !!}"></script>

<!--Beyond Scripts-->
<script src="{!! asset('assets/js/beyond.js') !!}"></script>


<!--Page Related Scripts-->
@if($sparkline_chart)
<!--Sparkline Charts Needed Scripts-->
<script src="{!! asset('assets/js/charts/sparkline/jquery.sparkline.js') !!}"></script>
<script src="{!! asset('assets/js/charts/sparkline/sparkline-init.js') !!}"></script>
@endif
@if($easy_pie_chart)
<!--Easy Pie Charts Needed Scripts-->
<script src="{!! asset('assets/js/charts/easypiechart/jquery.easypiechart.js') !!}"></script>
<script src="{!! asset('assets/js/charts/easypiechart/easypiechart-init.js') !!}"></script>
@endif
@if($flot_chart)
<!--Flot Charts Needed Scripts-->
<script src="{!! asset('assets/js/charts/flot/jquery.flot.js') !!}"></script>
<script src="{!! asset('assets/js/charts/flot/jquery.flot.resize.js') !!}"></script>
<script src="{!! asset('assets/js/charts/flot/jquery.flot.pie.js') !!}"></script>
<script src="{!! asset('assets/js/charts/flot/jquery.flot.tooltip.js') !!}"></script>
<script src="{!! asset('assets/js/charts/flot/jquery.flot.orderBars.js') !!}"></script>
@endif
<script src="{!! asset('assets/sweetalert/sweetalert.min.js') !!}"></script>
<script src="{!! asset('assets/js/select2.full.min.js') !!}"></script>
<script src="{!! asset('assets/js/panel.js') !!}"></script>
@yield('footer_script')
{{--End Script--}}
<script>
    $(document).ready(function () {
        $('[data-id^="deleted_form_"]').click(function () {
            var $me = $(this);
            function deleteFun() {
                $me.parents('form#'+$me.data('id')).submit()
            }

            swal({
                title: "Did you want to Delete?",
                type: "error",
                showCancelButton: true,
            }, function (inConfurm) {
                if (inConfurm) {
                    deleteFun()
                }
            })
        });
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
</body>
</html>
