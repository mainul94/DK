<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 3/25/16
 * Time: 4:53 PM
 */
 ?>

@if(!empty(session()->has('message')))
    <div class="col-xs-12">
        <div class="alert alert-{{session('message')['type']}} fade in">
            <button class="close" data-dismiss="alert">
                ×
            </button>
            <i class="fa-fw fa fa-check"></i>
            <strong>Success</strong> {!! session('message')['msg'] !!}
        </div>
    </div>
@endif


<div class="form-group col-xs-12 {{ $errors->has('organization_id') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('organization_id','Organization',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-10">
        @if(!empty($id))
            <?php $organization_id = [$id->organization->id => $id->organization->name]  ?>
        @else
            <?php $organization_id = []  ?>
        @endif
        {!! Form::select('organization_id',$organization_id,null,['class'=>'form-control']) !!}
        {!! $errors->first('organization_id','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-xs-12 {{ $errors->has('unit_id') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('unit_id','Unit',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-10">
        @if(!empty($id))
            <?php $unit_id = [$id->unit->id => $id->unit->name]  ?>
        @else
            <?php $unit_id = []  ?>
        @endif
        {!! Form::select('unit_id',$unit_id,null,['class'=>'form-control']) !!}
        {!! $errors->first('unit_id','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-xs-12 {{ $errors->has('division_id') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('division_id','Division',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-10">
        @if(!empty($id))
            <?php $division_id = [$id->division->id => $id->division->name]  ?>
        @else
            <?php $division_id = []  ?>
        @endif
        {!! Form::select('division_id',$division_id,null,['class'=>'form-control']) !!}
        {!! $errors->first('division_id','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-xs-12 {{ $errors->has('name') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('name','Name',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('name',null,['class'=>'form-control']) !!}
        {!! $errors->first('name','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group col-xs-12 {{ $errors->has('address') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('address','Address') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::textarea('address',null,['class'=>'form-control']) !!}
        {!! $errors->first('address','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="col-xs-12">
    <div class="col-xs-10 pull-right">
        <button type="submit" class="btn btn-blue">Save</button>
    </div>
{{--    <a href="{!! action('OrganizationController@index') !!}" class="btn btn-warning">Cancel</a>--}}
</div>


@section('footer_script')
    <script>
    getvalueForSelect2("select[name='organization_id']",'organizations',['id','name'],[],'id','name');
    getvalueForSelect2("select[name='unit_id']",'units',['id','name'],[['organization_id']],'id','name');
    getvalueForSelect2("select[name='division_id']",'divisions',['id','name'],[['organization_id'],['unit_id']],'id','name');
    </script>
@endsection