<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeePersonaInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_persona_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fathers_name');
            $table->string('mothers_name');
            $table->string('husband_name');
            $table->string('photo');
            $table->text('permanent_address');
            $table->text('bangla_permanent_address');
            $table->text('present_address');
            $table->text('bangla_present_address');
            $table->integer('contact_no');
            $table->integer('emergency_contact_no');
            $table->string('national_id',20);
            $table->string('nationality',100);
            $table->date('date_of_birth');
            $table->enum('religion',['Islam','Judaism','Christianity','Hinduism','Buddhism','Others'])->default('Islam');
            $table->enum('marital_status',['Yes','No']);
            $table->integer('number_of_children');
            $table->integer('employee_id',0,1);
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_persona_infos', function (Blueprint $table) {
            $table->dropForeign(['employee_id']);
            $table->dropForeign(['created_by']);
            $table->dropForeign(['updated_by']);
        });
        Schema::drop('employee_persona_infos');
    }
}
