<?php
/**
 * Created by PhpStorm.
 * User: raju
 * Date: 3/27/16
 * Time: 6:24 PM
 */?>

@if(!empty(session()->has('message')))
    <div class="col-xs-12">
        <div class="alert alert-{{session('message')['type']}} fade in">
            <button class="close" data-dismiss="alert">
                ×
            </button>
            <i class="fa-fw fa fa-check"></i>
            <strong>Success</strong> {!! session('message')['msg'] !!}
        </div>
    </div>
@endif


<div class="form-group col-xs-12 {{ $errors->has('employee_id') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('employee_id','Employee Id',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-10">
        @if(!empty($id))
            <?php $employee = [$id->employee->id => $id->employee->card_no]; $disabled = ['disabled'=>'disabled']  ?>
        @else
            <?php $employee = []; $disabled = []  ?>
        @endif
        {!! Form::select('employee_id',$employee,null,['class'=>'form-control']+$disabled) !!}
        {!! $errors->first('employee_id','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group col-xs-12">
    <div class="col-sm-2 text-right">
        {!! Form::label('date','Date') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::date('date',null,['class'=>'form-control','placeholder'=>'y-m-d']+$disabled) !!}

    </div>
</div>
<div class="form-group col-xs-12">
    <div class="col-sm-2 text-right">
        {!! Form::label('in_time','In Time') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('in_time',null,['class'=>'form-control']) !!}

    </div>
</div>
<div class="form-group col-xs-12">
    <div class="col-sm-2 text-right">
        {!! Form::label('out_time','Out Time') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('out_time',null,['class'=>'form-control']) !!}

    </div>
</div>
<div class="form-group col-xs-12">
    <div class="col-sm-2 text-right">
        {!! Form::label('duration','Duration') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('duration',null,['class'=>'form-control', 'readonly'=>'disabled'])!!}
    </div>
</div>
<div class="form-group col-xs-12 ">
    <div class="col-sm-2 text-right">
        {!! Form::label('let_time','Let Time') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('let_time',null,['class'=>'form-control', 'readonly'=>'disabled']) !!}
    </div>
</div>
<div class="form-group col-xs-12 ">
    <div class="col-sm-2 text-right">
        {!! Form::label('overtime','Overtime') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('overtime',null,['class'=>'form-control', 'readonly'=>'disabled']) !!}
    </div>
</div>
<div class="form-group col-xs-12 {{ $errors->has('status') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('status','Status') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('status',null,['class'=>'form-control', 'readonly'=>'disabled']) !!}
        {!! $errors->first('status','<span class="help-block">:message</span>') !!}
    </div>
</div>
<div class="form-group col-xs-12">
    <div class="col-sm-2 text-right">
        {!! Form::label('remark','Remark') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::textarea('remark',null,['class'=>'form-control']) !!}
    </div>
</div>
<div class="col-xs-12 attendance">
    <div class="col-xs-10 pull-right">
        <button type="submit" class="btn btn-blue">Save</button>
    </div>
    {{--    <a href="{!! action('OrganizationController@index') !!}" class="btn btn-warning">Cancel</a>--}}
</div>

@section('footer_script')
    <script>
        getvalueForSelect2("select[name='employee_id']",'employees',['id','card_no'],[],'id','card_no');

        //        Get Employee Details
        $("select[name='employee_id']").on('change', function () {
            setEmpDetails($(this).val())
        });
        function setEmpDetails(emp) {
            if (typeof emp === "undefined" || emp ==null) {
                return
            }
            $.ajax({
                url:"{!! url('/employee') !!}"+"/"+emp,
                success: function (data) {
                    var $view = $("#employee_details");

//                    PHoto
                    $view.find('.photo').html('<img class="img-responsive" src="{!! asset('/') !!}'+data.photo+'">');
//                    Details
                    $view.find('.details').append('<h4>'+data.name+'</h4>');
                    $view.find('.details').append('<div class="subTitle"></div>');
                    getValue("{!! url('/section') !!}/"+data.section_id, function (r) {
                        $view.find('.subTitle').append(r.name + '<br>')
                    });
                    getValue("{!! url('/line') !!}/"+data.line_id, function (r) {
                        $view.find('.subTitle').append(r.name + '<br>')
                    });
                    getValue("{!! url('/designation') !!}/"+data.designation_id, function (r) {
                        $view.find('.subTitle').append(r.name + '<br>')
                    });
                    $view.find('.details').append(data.date_of_joining);
                },
                complete: function (xhr) {
//                    console.log(xhr)
                }
            });
        }
        setEmpDetails($("select[name='employee_id']").val())

//        Call Calculation for Late Time OverTime Status
        var settings = {!! $settings !!}
            //time calculation
                $("#out_time").change(function () {
                    var intime = $("#in_time").val();
                    var outtime = $("#out_time").val();

                    setDuration(intime, outtime);
                    setOvertime(intime, outtime, ('office_duration_time' in settings ? settings.office_duration_time:''));
                });
        //time calculation
        $("#in_time").change(function () {
            var intime = $("#in_time").val();
            var outtime = $("#out_time").val();
            setLate(intime, ('office_opening_time' in settings ? settings.office_opening_time:''), ('attendance_buffer_time' in settings ? settings.attendance_buffer_time:''));
            setDuration(intime, outtime);
            setOvertime(intime, outtime, ('office_duration_time' in settings ? settings.office_duration_time:''));
        });

        $('#duration').on('change', function () {
            if ('lunch_break' in settings) {
                var lunchbrackSplit = settings.lunch_break.split('-');
                var lunchDuration = toSeconds(lunchbrackSplit[0],lunchbrackSplit[1])

                var duration_array = false;
                if (toSeconds($('#out_time').val())<=toSeconds(lunchbrackSplit[0])) {
                    duration_array = calDuration('00:00:00', $(this).val());
                }else if (toSeconds($('#out_time').val())<=toSeconds(lunchbrackSplit[1])) {
                    var tmp_duration_array = calDuration(lunchbrackSplit[0], $('#out_time').val());
                    var tmp_du = tmp_duration_array.map(function(v) {
                        return v < 10 ? '0' + v : v;
                    }).join(':');
                    duration_array = calDuration(tmp_du, $(this).val());
                }else {
                    var tmp_duration_array = calDuration(lunchbrackSplit[0], lunchbrackSplit[1]);
                    var tmp_du = tmp_duration_array.map(function(v) {
                        return v < 10 ? '0' + v : v;
                    }).join(':');
                    duration_array = calDuration(tmp_du, $(this).val());
                }

                if (duration_array) {
                    // 0 padding and concatation
                    duration = duration_array.map(function(v) {
                        return v < 10 ? '0' + v : v;
                    }).join(':');
                    $(this).val(duration);
                }

            }
            if (!('min_working_hours' in settings)) {
                return false
            }

            if (toSeconds($(this).val()) < toSeconds(settings.min_working_hours)) {
                $('#status').val('Absent')
            }else {
                $('#status').val('Present')
            }
        });
    </script>
@endsection