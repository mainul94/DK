<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 3/25/16
 * Time: 4:05 PM
 */
$sl = 1;
?>


@extends('layouts.layout')


@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="well with-header">
                <div class="header bg-info">
                    <div class="row">
                        <div class="col-sm-7 col-xs-12">
                            Setting List
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 text-right padding-bottom-20px">

                </div>
                @if(!empty(session()->has('message')))
                    <div class="col-xs-12">
                        <div class="alert alert-{{session('message')['type']}} fade in">
                            <button class="close" data-dismiss="alert">
                                ×
                            </button>
                            <i class="fa-fw fa fa-check"></i>
                            <strong>Success</strong> {!! session('message')['msg'] !!}
                        </div>
                    </div>
                @endif
                @if(count($setting)>0)
                    <table class="table table-hover table-bordered">
                        <thead class="bordered-blue">
                        <tr>
                            <th>Sl.No</th>
                            <th>Property</th>
                            <th>Value</th>
                            <th>Created By</th>
                            <th>updated By</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($setting as $setting)
                            <tr>
                                <td>{!! $sl !!} <?php $sl++; ?></td>
                                <td>{!! $setting->property !!}</td>
                                <td>{!! $setting->value !!}</td>
                                <td>{!! $setting->createdBy->name !!}</td>
                                <td>
                                    @if($setting->updateBy)
                                        {!! $setting->updateBy->name !!}
                                    @endif
                                </td>
                                <td class="text-center">
                                    <a href="{!! action('SettingController@edit',$setting->id) !!}" ><i class="fa fa-pencil-square-o text-danger"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <h3 class="text-muted text-center">
                        No Data
                    </h3>
                @endif
            </div>

        </div>
    </div>
@endsection