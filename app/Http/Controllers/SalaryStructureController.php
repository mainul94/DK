<?php

namespace App\Http\Controllers;

use App\SalarySetting;
use App\SalaryStructure;
use App\SalaryStructureChild;
use Illuminate\Http\Request;

use App\Http\Requests;

class SalaryStructureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rows = SalaryStructure::all();
        return view('salary_structure.listview',compact('rows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('salary_structure.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'employee_id'=>'required'
        ]);
        $values = $request->all();
        $values['created_by'] = $request->user()->id;
        $this->storeData($values);

        if ($request->ajax()) {
            return response()->json([
                'message'=>'Success'
            ]);
        }

        return redirect()->back()->with('message',['type'=>'success','msg'=>'Successfully Saved']);
    }


    /**
     * Store Date after Validation
     * @param $values
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function storeData($values)
    {
        $salaryStructure = new SalaryStructure;
        $salaryStructure->fill($values)->save();

        $child_val = ['salary_structure_id'=>$salaryStructure->id];
        foreach ($values['salary_type_id'] as $key => $val) {
            $child_val['salary_type_id'] = $val;
            $child_val['amount'] = $values['amount'][$key];
            SalaryStructureChild::create($child_val);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SalaryStructure $id)
    {
        $salratySetting = SalarySetting::where('designation_id',$id->employee->designation_id)
            ->where('employee_type_id',$id->employee->employee_type_id)->first();
        $types = [];
        foreach ($salratySetting->children as $ley=>$setting) {
            $child = $id->children->where('salary_type_id',$setting->salary_type_id)->first();
                $types[$ley] = [
                "ch_id" => $child->id,
                "salary_structure_id" => $child->salary_structure_id,
                "salary_type_id" => $child->salary_type_id,
                "amount" => $child->amount,
                "min_amount" => $setting->min_amount,
                "max_amount" => $setting->max_amount
            ];

        }
        $types = collect($types);
        return view('salary_structure.edit',compact('id','types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SalaryStructure $id)
    {
        $values = $request->all();
        $values['updated_by']=$request->user()->id;
        $id->fill($values);
        $id->save();
        foreach ($values['ch_id'] as $key=>$val) {
            $salary_structure_child = SalaryStructureChild::find($val);
            $salary_structure_child->fill(['amount'=>$values['amount'][$key]])->save();
        }
        return redirect()->to(action('SalaryStructureController@edit',$id))->with('message',['type'=>'success','msg'=>'Successfully Updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id->delete();
        return redirect()->to(action('SalaryStructureController@index'))->with('message',['type'=>'danger','msg'=>'Successfully Deleted']);
    }
}
