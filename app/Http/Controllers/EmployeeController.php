<?php

namespace App\Http\Controllers;

use App\Designation;
use App\Employee;
use App\EmployeeOrgProfile;
use App\EmployeePersonaInfo;
use App\ImportEmpoyee;
use App\Setting;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Storage;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->get('employee_id')) {
            $rows = Employee::findOrFail($request->get('employee_id'));
            $employees = [$rows->id,$rows->card_no];
            $employee_id = $rows->id;
        }else {
            $rows = Employee::paginate(15);
        }
        return view('employee.listview',compact('rows','employees','employee_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $card_no = Employee::orderBy('id','desc')->first()['card_no'];
        return view('employee.create',compact('card_no'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validation($request);
        $values = $request->all();
        $values['created_by'] = $request->user()->id;
        if ($request->hasFile('photo')) {
            Storage::disk('public')->put(
                'avatars/'.$request->file('photo')->getClientOriginalName(),
                file_get_contents($request->file('photo')->getRealPath())
            );
            $values['photo'] = 'avatars/'.$request->file('photo')->getClientOriginalName();
        }
        $this->employeeStore($values);
        if ($request->ajax()) {
            return response()->json([
                'message'=>'Success'
            ]);
        }

        return redirect()->back()->with('message',['type'=>'success','msg'=>'Successfully Saved']);
    }


    /**
     * @param $values
     */
    public function employeeStore($values)
    {
        $setValue = [
            'organization_id','unit_id','division_id','department_id','section_id','line_id','designation_group_id'
        ];
        if (!empty($values['designation_id'])) {
            $designation = Designation::find($values['designation_id'])->toArray();
            foreach ($setValue as $key) {
                $values[$key] = $designation[$key];
            }
        }
        $card_no = Employee::orderBy('id','desc')->first()['card_no'];
        $setting_card_no = Setting::where('property','=','employee_card_no_start_form')->first()['value'];
        if ($card_no && $card_no >= $setting_card_no) {
            $values['card_no']=$card_no+1;
        }else if($card_no){
            $values['card_no']=$setting_card_no+1+$card_no;
        }else {
            $values['card_no']=$setting_card_no+1;
        }
        $emp = new Employee;
        $emp->fill($values)->save();
//        $values['employee_id'] = $emp->id;
//        EmployeeOrgProfile::create($values);
//        EmployeePersonaInfo::create($values);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,Employee $id)
    {
//        $data = $id->toArray();
//        $dd = $id->getEmployeePersonalInfo? $id->getEmployeePersonalInfo->toArray():[];
//        $ddd = $id->getEmployeeOrganizationInfo?$id->getEmployeeOrganizationInfo->toArray():[];
        if ($request->ajax()) {
//            return response()->json($data+$dd+$ddd);
            return response()->json($id->toArray());
        }
        return view('employee.view',compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('employee.edit',compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validation($request);
        $values = $request->all();
        $values['updated_by'] = $request->user()->id;
        if ($request->hasFile('photo')) {
            Storage::disk('public')->put(
                'avatars/'.$request->file('photo')->getClientOriginalName(),
                file_get_contents($request->file('photo')->getRealPath())
            );
            $values['photo'] = 'avatars/'.$request->file('photo')->getClientOriginalName();
        }
        $this->updateEmployee($id,$values);
        return redirect()->to(action('EmployeeController@edit',$id))->with('message',['type'=>'success','msg'=>'Successfully Updated']);
    }


    /**
     * @param $id
     * @param $values
     */
    public function updateEmployee($id,$values)
    {
        $id->fill($values);
        $id->save();
//        $values['employee_id'] =$id->id;
//        $personal = EmployeePersonaInfo::where('employee_id',$id->id)->first();
//        if (empty($personal)) {
//            $personal = new EmployeePersonaInfo;
//            $personal->fill(['created_by'=>$values['updated_by']]);
//        }
//        $personal->fill($values);
//        $personal->save();
//        $org = EmployeeOrgProfile::where('employee_id',$id->id)->first();
//        if (empty($org)) {
//            $org = new EmployeeOrgProfile;
//            $org->fill(['created_by'=>$values['updated_by']]);
//        }
//        $org->fill($values);
//        $org->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id->delete();
        return redirect()->to(action('EmployeeController@index'))->with('message',['type'=>'danger','msg'=>'Successfully Deleted']);
    }

    private function validation($request)
    {
        $this->validate($request,[
            'name' => 'required',
//            'card_no' => 'required',
//            'organization_id' => 'required',
//            'unit_id' => 'required',
//            'division_id' => 'required',
//            'department_id' => 'required',
//            'section_id' => 'required',
//            'line_id' => 'required',
//            'designation_group_id' => 'required',
            'designation_id' => 'required',
            'employee_category_id' => 'required',
            'employee_type_id' => 'required',
            'holiday_id' => 'required',
            'basic_salary'=>'numeric|between:'.$request->get('min_basic').','.$request->get('max_basic'),
            'probation_period_id' => 'required'
        ]);
    }


    public function viewEmployeeImport()
    {
        return view('employee.import');
    }

    public function importEmployee(Request $request)
    {
        $this->findIds = ['organization'=>'\App\Organization','unit'=>'\App\Unit','department'=>'\App\Department',
            'division'=>'\App\Division','section'=>'\App\Section','line'=>'\App\Line'];
        $this->updatedBy = $request->user()->id;
//        try {
            $excle = ImportEmpoyee::load($request->file('file'),function($reander){
                dd($reander->get());
                foreach ($reander->get() as $key=>$row) {
                    $row = ImportEmpoyee::createDesignationAndSetRowValues($row,$this->updatedBy);

                    foreach ($row as $k=>$v) {
                        if (is_null($v)) {
                            $row->forget($k);
                        }
                    }
                    $employee = Employee::where([['name','=',$row->name],['old_card_no','=',$row->card_no]])->first();
//                    dd($employee);
                    if (!empty($employee)){
                        $row['updated_by'] = $this->updatedBy;
                        $this->updateEmployee($employee,$row->toArray());
                    }
                    else {
                        $row['created_by'] = $this->updatedBy;
                        $this->employeeStore($row->toArray());
                    }

                }
            });
//        } catch (Exception $e) {
//            echo 'Caught exception: ',  $e->getMessage(), "\n";
//        }

        return redirect()->back()->with('message',['type'=>'success','msg'=>'Successfully Saved']);
    }
    
}
