<?php

namespace App;

use App\Model\CommonField;
use Illuminate\Database\Eloquent\Model;

class HolidayChild extends Model
{
    protected $fillable = ['name','holiday_type_id','holiday_id','date','details','created_by','updated_by'];


    use CommonField;


    public function type()
    {
        return $this->belongsTo('App\HolidayType','holiday_type_id');
    }

}
