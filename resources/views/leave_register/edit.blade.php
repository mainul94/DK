<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 3/25/16
 * Time: 4:52 PM
 */
?>

@extends('layouts.layout')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-bottom bordered-blue">
                <span class="widget-caption">Edit Employee Leave Register</span>
            </div>
            <div class="widget-body">
                <div class="row">
                    {!! Form::model($id,['action'=>['LeaveRegisterController@update',$id->id],'method'=>'PATCH']) !!}
                    <div class="col-sm-8">
                        @include('leave_register._form')
                    </div>
                    <div class="col-sm-4">
                        @include('leave_register._available_leave_days')
                    </div>
                    <div class="col-sm-12">
                        @include('leave_register._child_rows')
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
