<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>DK HRM</title>

    <meta name="description" content="login page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="{!! asset('assets/img/favicon.png') !!}" type="image/x-icon">

    <!--Basic Styles-->
    <link href="{!! asset('assets/css/bootstrap.min.css') !!}" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="{!! asset('') !!}#" rel="stylesheet" />
    <link href="{!! asset('assets/css/font-awesome.min.css') !!}" rel="stylesheet" />

    <!--Fonts-->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css">

    <!--Beyond styles-->
    <link id="beyond-link" href="{!! asset('assets/css/beyond.min.css') !!}" rel="stylesheet" />
    <link href="{!! asset('assets/css/demo.min.css') !!}" rel="stylesheet" />
    <link href="{!! asset('assets/css/animate.min.css') !!}" rel="stylesheet" />
    <link id="skin-link" href="{!! asset('') !!}#" rel="stylesheet" type="text/css" />

    <!--Skin Script: Place this script in head to load scripts for skins and rtl support-->
    <script src="{!! asset('assets/js/skins.min.js') !!}"></script>
</head>
<!--Head Ends-->
<!--Body-->
<body>
@yield('content')

<!--Basic Scripts-->
<script src="{!! asset('assets/js/jquery.min.js') !!}"></script>
<script src="{!! asset('assets/js/bootstrap.min.js') !!}"></script>
<script src="{!! asset('assets/js/slimscroll/jquery.slimscroll.min.js') !!}"></script>

<!--Beyond Scripts-->
<script src="{!! asset('assets/js/beyond.js') !!}"></script>


</body>
</html>
