<?php

namespace App;

use App\Model\CommonField;
use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $fillable = ['name','address','organization_id','created_by','updated_by'];

    use CommonField;

}
