<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 6/14/16
 * Time: 12:14 AM
 */
$sl = 1;
if (!empty($_REQUEST['page'])) {
    $sl += ($_REQUEST['page']-1) *15;
}
?>


@extends('layouts.layout')


@section('content')
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="widget">
                <div class="widget-header ">
                    <span class="widget-caption">Salary Report</span>
                </div>
                <div class="widget-body">
                    <div class="col-xs-12">
                        {!! Form::open(['action'=>['ReportController@salary'],'method'=>'GET']) !!}
                        <div class="form-group col-xs-4 {{ $errors->has('month') ? 'has-error':''}}">
                            <div class="col-sm-4 text-right">
                                {!! Form::label('month','Month',['class'=>'field-required']) !!}
                            </div>
                            <div class="col-sm-8">
                                {!! Form::select('month',$months,$month,['class'=>'form-control']) !!}
                                {!! $errors->first('month','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="form-group col-xs-3 {{ $errors->has('year') ? 'has-error':''}}">
                            <div class="col-sm-4 text-right">
                                {!! Form::label('year','Year',['class'=>'field-required']) !!}
                            </div>
                            <div class="col-sm-8">
                                {!! Form::select('year',$years,$year,['class'=>'form-control']) !!}
                                {!! $errors->first('year','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="form-group col-xs-3 {{ $errors->has('section_id') ? 'has-error':''}}">
                            <div class="col-sm-4 text-right">
                                {!! Form::label('section_id','Section') !!}
                            </div>
                            <div class="col-sm-8">
                                {!! Form::select('section_id',$sections,$section_id,['class'=>'form-control']) !!}
                                {!! $errors->first('section_id','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="col-xs-2">
                            <div class="col-xs-12 pull-right">
                                <button type="submit" class="btn btn-info">Filter</button>
                                <a class="btn btn-ifo pull-right" href="{!! action("ReportController@salary",['comp'=> $comp, 'year'=> $year, 'month'=> $month,
                                     'section_id'=> $section_id, 'view_file'=>$comp?'pdf.salary':'pdf.ext_salary']) !!}">PDF</a>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="table-responsive reportForPrint col-xs-12">
                        <table class="table table-striped table-bordered table-hover"  id="salaryReport">
                            <caption class="text-center">
							<h2>DK Global Fashion Ware Limited</h2>
                                <p> Salary Wages Sheet for the Month of:  {{ array_key_exists($month,$months)?$months[$month]:'' }} - {{ array_key_exists($year,$years)?$years[$year]:'' }}</p>
                            </caption>
                            <thead>
                            <tr>
                                <th style="vertical-align: top;">Sl No</th>
                                <th style="vertical-align: top;">ID No</th>
                                <th style="vertical-align: top;">Name</th>
                                <th style="vertical-align: top;">Designation</th>
                                <th  style="vertical-align: top; min-width: 50px;">
                                    Grade
                                    <hr>
                                    DOJ
                                </th>

                                @if(!empty($comp))  
                                <th style="vertical-align: top;">
                                    Gross Salary
                                </th>
                                @endif
                                <th style="vertical-align: top;">Basic</th>
                                @if(!empty($comp))
                                    <th style="vertical-align: top;">
                                        H Rent
                                        <hr>
                                        Med.
                                    </th>
                                    <th style="vertical-align: top;">
                                        Food
                                        <hr>
                                        Conv
                                    </th>
                                    <th style="vertical-align: top;">
                                        T Day
                                        <hr>
                                        H Day
                                    </th>
                                @endif
                                <th style="vertical-align: top;">
                                    P Days
                                    <hr>
                                    Ab Days
                                </th>
                                @if(empty($comp))
                                <th style="vertical-align: top;">All Days</th>
                                @endif
                                <th style="vertical-align: top;">Att Pay Days</th>
                                <th style="vertical-align: top;">Att Bonus</th>
                                <th style="vertical-align: top;">Ab Ded</th>
                                <th style="vertical-align: top;">OT Rate</th>
                                <th style="vertical-align: top;">OT Hours</th>
                                <th style="vertical-align: top;">OT Amount</th>
                                <th style="vertical-align: top;">Holly Day All</th>
                                <th style="vertical-align: top;">Arrear</th>
                                <th style="vertical-align: top;">Fest Bonus</th>
                                <th style="vertical-align: top;">Mobile Bill</th>
                                <th style="vertical-align: top;">Special Bill</th>
                                <th style="vertical-align: top;">Car Bill</th>
                                <th style="vertical-align: top;">Others</th>
                                <th style="vertical-align: top;">Incentive</th>
                                <th style="vertical-align: top;">Adv</th>
                                <th style="vertical-align: top;">Others Ded</th>
                                <th style="vertical-align: top;">Tax</th>
                                <th style="vertical-align: top;">Revenue Stamp</th>
                                <th style="vertical-align: top;">Payable amount</th>
                                <th style="vertical-align: top;">Signature</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($rows as $row)
                                    <tr>
                                        <td>{!! $sl !!} {{--*/ $sl++ /*--}}</td>
                                        <td>{!! $row->employee->old_card_no !!}</td>
                                        <td>{!! $row->employee->name !!}</td>
                                        <td>{!! $row->employee->designation->name !!}</td>
                                        <td>
                                            {!! $row->employee->designation->grade !!}
                                            <hr>
                                            {!! date('d-M-y',strtotime($row->employee->date_of_joining)) !!}
                                        </td>
                                        @if(!empty($comp))
                                        <td>{!! $row->gross_salary !!}</td>
                                        @endif
                                        <td>{!! $row->basic_salary !!}</td>
                                        @if(!empty($comp))
                                            <td>
                                                {!! $row->hra !!}
                                                <hr>
                                                {!! $row->medical !!}
                                            </td>
                                            <td>
                                                {!! $row->food_all !!}
                                                <hr>
                                                {!! $row->conveyance !!}
                                            </td>
                                            <td>
                                                {!! $row->total_days !!}
                                                <hr>
                                                {{--*/ $holidays = App\HolidayChild::where('holiday_id',$row->employee->holiday->id)
                                                        ->whereMonth('date','=',$row->month)->count() /*--}}
                                                {!! $holidays !!}
                                            </td>
                                        @endif
                                        <td>
                                            {!! $row->p_days !!}
                                            <hr>
                                            {!! $row->ab_days !!}
                                        </td>
                                        @if(empty($comp))
                                        <td>{!! $row->total_days !!}</td>
                                        @endif
                                        <td>{!! $row->payable_days !!}</td>
                                        <td>{!! $row->attendance_bonus !!}</td>
                                        <td>{!! $row->ab_deduction !!}</td>
                                        <td>{!! round($row->ot_rate,2) !!}</td>
                                        @if(!empty($comp))
                                        <td>{!! $row->ot_hours !!}</td>
                                        <td>{!! $row->ot_amount !!}</td>
                                        @else
                                        <td>{!! $row->ext_ot_hours !!}</td>
                                        <td>{!! $row->ext_ot_amount !!}</td>
                                        @endif
                                        <td>{!! $row->holiday_all !!}</td>
                                        <td>{!! $row->arrear !!}</td>
                                        <td>{!! $row->fest_bonus !!}</td>
                                        <td>{!! $row->mobile_bill !!}</td>
                                        <td>{!! $row->special_bill !!}</td>
                                        <td>{!! $row->car_bill !!}</td>
                                        <td>{!! $row->others !!}</td>
                                        <td>{!! $row->incentive !!}</td>
                                        <td>{!! $row->advance !!}</td>
                                        <td>{!! $row->others_deduction !!}</td>
                                        <td>{!! $row->tax !!}</td>
                                        <td>{!! $row->revenue_stamp !!}</td>
                                        @if(!empty($comp))
                                        <td>{!! $row->payable_amount - $row->ot_amount !!}</td>
                                        @else
                                        <td>{!! $row->payable_amount - $row->ext_ot_amount !!}</td>
                                        @endif
                                        <td></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
{{--                    {!! $rows->render() !!}--}}
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('style_sheet')
    <style>
        #salaryReport hr {
            margin: 5px 0;
        }
        #salaryReport th {
            min-width: 60px;
        }
    </style>
@endsection

@section('footer_script')
    <script src="{{ url('assets/js/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('assets/js/datatable/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ url('assets/js/datatable/dataTables.bootstrap.min.js') }}"></script>
    <script>
        getvalueForSelect2("select[name='section_id']",'sections',['id','name'],[],'id','name');
        var InitiateSalaryReportTable = function() {
            return {
                init: function() {
                    var oTable = $('#salaryReport').dataTable({
                        "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                        // "aLengthMenu": [
                        //     [5, 15, 20, -1],
                        //     [5, 15, 20, "All"]
                        // ],
                        // "iDisplayLength": 10,
                        "oTableTools": {
                            "aButtons": [
                                "print"
                            ]
                        },
                        "language": {
                            "search": "",
                            "sLengthMenu": "_MENU_",
                            "oPaginate": {
                                "sPrevious": "Prev",
                                "sNext": "Next"
                            }
                        },
                        "searching": false,
                        "paging":   false,
                        "ordering": false,
                        "info":     false
                    });

                    /*$("tfoot input").keyup(function() {
                        /!* Filter on the column (the index) of this element *!/
                        oTable.fnFilter(this.value, $("tfoot input").index(this));
                    });*/
                    $('.DTTT_button_print').on('click',function (event) {
                        event.preventDefault();
                        window.print();
                    });
                }
            };
        }();
        InitiateSalaryReportTable.init();
    </script>
@endsection