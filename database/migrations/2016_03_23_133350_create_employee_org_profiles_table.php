<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeOrgProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_org_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_of_nominees');
            $table->text('address_of_nominees');
            $table->string('relation_with_nominees');
            $table->string('authorized_percent_for_nominees');
            $table->string('last_organization');
            $table->date('date_of_joining');
            $table->float('joining_salary');
            $table->float('current_salary');
            $table->date('increment_month');
            $table->text('reason_of_separation');
            $table->date('date_of_separation');
            $table->date('confirm_date');
            $table->enum('payment_mode',['Cash','Bank']);
            $table->string('bank_name');
            $table->string('branch_name');
            $table->string('account_holder');
            $table->string('account_no');
            $table->integer('employee_id',0,1);
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_org_profiles', function (Blueprint $table) {
            $table->dropForeign(['employee_id']);
            $table->dropForeign(['created_by']);
            $table->dropForeign(['updated_by']);
        });

        Schema::drop('employee_org_profiles');
    }
}
