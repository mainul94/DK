<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 7/24/16
 * Time: 9:00 PM
 */
?>
@extends('layouts.layout')

@section('content')

    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="widget">
                <div class="widget-header ">
                    <span class="widget-caption">ID Card</span>
                </div>
                <div class="widget-body">
                    <div class="col-xs-12 filter-area">
                        {!! Form::open(['action'=>['ReportController@idCard'],'method'=>'GET']) !!}
                        <div class="form-group col-xs-4 {{ $errors->has('employee_id') ? 'has-error':''}}">
                            <div class="col-sm-4 text-right">
                                {!! Form::label('employee_id','Employee') !!}
                            </div>
                            <div class="col-sm-8">
                                {!! Form::select('employee_id',$employees,$employee_id,['class'=>'form-control']) !!}
                                {!! $errors->first('employee_id','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="col-xs-1">
                            <div class="col-xs-10 pull-right">
                                <button type="submit" class="btn btn-info">Filter</button>
                            </div>
                        </div>
                        {!! Form::close() !!}

                    </div>
                    <div class="col-xs-12 id-card">
                        <div class="page-break">
                        @foreach($rows as  $key=> $row)
                            <div class="col-xs-6 item" style="margin-top: 2em;">
                                <div class="col-xs-12" style="border:1px solid gray">
                                    <div class="row" style="border-bottom: 1px solid gray">
                                        <p class="col-xs-6">আই ডি কার্ড নং  {!! $row->old_card_no !!}</p>
                                        <p class="col-xs-6">ইস্যুর তারিখ  {!! date('d-M-Y') !!}</p>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-8">
                                            <p> কারখানা/প্রতিষ্ঠানের নাম : {!! $row->organization->name !!}</p>
                                            <p> শ্রমিকের নাম : {!! $row->name !!}</p>
                                            <p> পদবি : {!! $row->designation->name !!}</p>
                                            <p> বিভাগ/শাখা : {!! $row->department->name !!}</p>
                                            <p> যোগদানের তারিখ : {!! date('d-M-Y',strtotime($row->date_of_joining)) !!}</p>
                                        </div>
                                        <div class="col-xs-4">
                                            @if($row->photo)
                                                <br>
                                                <img src="{!! url($row->photo) !!}" alt="Image" class="img-responsive img-thumbnail">
                                            @endif
                                        </div>
                                        <div class="col-xs-8">
                                            <br>
                                            <br>
                                            <p>শ্রমিকের স্বাক্ষর </p>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p style="margin-top: 1em;"> স্বাক্ষর <br> মালিক/ব্যাবস্থাপক</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if(++$key%2 == 0)
                                <div class="clearfix"></div>
                            @endif
                            @if(++$key%8 == 0)
                                </div><div class="page-break">
                                {{--<p><!-- pagebresak --></p>--}}
                            @endif

                        @endforeach
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_script')
    <script>
        getvalueForSelect2("select[name='employee_id']",'employees',['id','name'],[],'id','name');
    </script>
@endsection