<?php
/**
 * Created by PhpStorm.
 * User: raju
 * Date: 3/27/16
 * Time: 3:39 PM
 */
?>

@if(!empty(session()->has('message')))
    <div class="col-xs-12">
        <div class="alert alert-{{session('message')['type']}} fade in">
            <button class="close" data-dismiss="alert">
                ×
            </button>
            <i class="fa-fw fa fa-check"></i>
            <strong>Success</strong> {!! session('message')['msg'] !!}
        </div>
    </div>
@endif


<div class="form-group col-xs-12 {{ $errors->has('property') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('property','Property',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('property',null,['class'=>'form-control', 'disabled'=>'1']) !!}
        {!! $errors->first('property','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group col-xs-12 {{ $errors->has('value') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('value','Value',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-10">
        {!! Form::textarea('value',null,['class'=>'form-control']) !!}
        {!! $errors->first('value','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="col-xs-12">
    <div class="col-xs-10 pull-right">
        <button type="submit" class="btn btn-blue">Save</button>
    </div>
    {{--    <a href="{!! action('OrganizationController@index') !!}" class="btn btn-warning">Cancel</a>--}}
</div>

