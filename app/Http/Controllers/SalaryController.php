<?php

namespace App\Http\Controllers;

use App\Salary;
use App\SalarySetting;
use Illuminate\Http\Request;

use App\Http\Requests;

class SalaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $year = $request->get('year')?$request->get('year'):date('Y');
        $month = $request->get('month')?$request->get('month'):date('m');
        $rows = Salary::where([['year','=',$year],['month','=',$month]])->get();

        return view('salary.listview',compact('rows','month','year'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $year = date('Y');
        $month = date('m');

        return view('salary.create',compact('year','month'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'month' => 'required|numeric|max:12',
            'year' => 'required|'
        ]);
        $salary = new Salary;
        $messages = $salary->multipleSalaryGenarate($request);

        return redirect()->back()->withInput()->with('message',$messages);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Salary $id)
    {
        return view('salary.edit',compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Salary $id)
    {
        $values = $request->all();
        $leaves = ['total'=>$values['total_leave'], 'payable' => $values['payable_leave'], 'notPayable' => $values['without_pay_leave']];
        $attendance = [
            'present'=>$values['p_days'],
            'absent'=>$values['ab_days'],
            'otHours'=>$values['ot_hours'],
            'extOtHours'=>$values['ext_ot_hours']
        ];
        $salarySetting = SalarySetting::where([['designation_id','=',$id->employee->designation_id],
            ['employee_type_id','=',$id->employee->employee_type_id]])->first();

        $values = $id->valueReArrange($id->employee, $values, $values['holiday_all'], $leaves, $attendance, $values['total_days'],
            $salarySetting);
        $id->fill($values)->save();
        return redirect()->back()->with('message',['type'=>'success','msg'=>'Successfully Updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
