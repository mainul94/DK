<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 7/26/16
 * Time: 8:28 PM
 */
$last_present = \App\Attendance::where([['date','<=',$date],['status','=','Present'],['employee_id','=',$employee->id]])
        ->orderBy('date','desc')->take(1)->get();
?>
@if(count($attendance) <= 0)
    <tr>
        <td>@if(!empty($show_sec)) {!! $employee->section->name !!}@endif</td>
        <td>{!! $employee->card_no !!}</td>
        <td>{!! $employee->name !!}</td>
        <td>{!! $designation->name !!}</td>
        <td>{!! $employee->in_time !!}</td>
        @if(count($last_present)>0)
            <td>{!! date('d-M-Y',strtotime($last_present[0]->date)) !!}</td>
            <td>{!! \Carbon\Carbon::parse($date)->diffInDays(\Carbon\Carbon::parse($last_present[0]->date)) !!}</td>
        @else
            <td></td>
            <td></td>
        @endif
        {{--<td></td>--}}
        <td>Absent</td>
        <td></td>
    </tr>
@endif
