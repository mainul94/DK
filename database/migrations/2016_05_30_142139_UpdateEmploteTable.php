<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEmploteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->string('fathers_name')->after('sex');
            $table->string('mothers_name')->after('sex');
            $table->string('husband_name')->after('sex');
            $table->string('photo')->after('sex');
            $table->text('permanent_address')->after('sex');
            $table->text('bangla_permanent_address')->after('sex');
            $table->text('present_address')->after('sex');
            $table->text('bangla_present_address')->after('sex');
            $table->integer('contact_no')->after('sex');
            $table->integer('emergency_contact_no')->after('sex');
            $table->string('national_id',20)->after('sex');
            $table->string('nationality',100)->after('sex');
            $table->date('date_of_birth')->after('sex');
            $table->enum('religion',['Islam','Judaism','Christianity','Hinduism','Buddhism','Others'])->default('Islam')->after('sex');
            $table->enum('marital_status',['Yes','No'])->after('sex');
            $table->integer('number_of_children')->after('sex');
            $table->string('name_of_nominees')->after('sex');
            $table->text('address_of_nominees')->after('sex');
            $table->string('relation_with_nominees')->after('sex');
            $table->string('authorized_percent_for_nominees')->after('sex');
            $table->string('last_organization')->after('sex');
            $table->date('date_of_joining')->after('sex');
            $table->float('joining_salary')->after('sex');
            $table->float('current_salary')->after('sex');
            $table->integer('increment_month')->after('sex');
            $table->text('reason_of_separation')->after('sex');
            $table->date('date_of_separation')->after('sex');
            $table->date('confirm_date')->after('sex');
            $table->enum('payment_mode',['Cash','Bank'])->after('sex');
            $table->string('bank_name')->after('sex');
            $table->string('branch_name')->after('sex');
            $table->string('account_holder')->after('sex');
            $table->string('account_no')->after('sex');

            $table->integer('gross_salary')->after('sex');
            $table->integer('basic_salary')->after('sex');
            $table->integer('hra')->after('sex');
            $table->integer('medical')->default(250)->after('sex');
            $table->integer('food_all')->default(650)->after('sex');
            $table->integer('conveyance')->default(200)->after('sex');
            $table->integer('mobile_bill')->after('sex');
            $table->integer('special_bill')->after('sex');
            $table->integer('car_bill')->after('sex');
            $table->integer('others')->after('sex');
            $table->integer('incentive')->after('sex');
            $table->enum('ot_status',['Yes','No'])->after('sex');
            $table->enum('attendance_bonus',['Yes','No'])->after('sex')->nullable();
            $table->enum('status',['Active','Inactive'])->default('Active')->after('sex');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->dropColumn([
                'fathers_name','mothers_name','husband_name','photo','permanent_address','bangla_permanent_address',
                'present_address','bangla_present_address','contact_no','emergency_contact_no','national_id','nationality',
                'date_of_birth','religion','marital_status','number_of_children','name_of_nominees','address_of_nominees',
                'relation_with_nominees','authorized_percent_for_nominees','last_organization','date_of_joining','joining_salary',
                'current_salary','increment_month','reason_of_separation','date_of_separation','confirm_date','payment_mode',
                'bank_name','branch_name','account_holder','account_no','gross_salary','basic_salary','hra','medical','food_all',
                'conveyance','mobile_bill','special_bill','car_bill','others','ot_status','attendance_bonus','incentive',
                'status'
            ]);
        });
    }
}
