<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 4/8/16
 * Time: 6:09 PM
 */
?>

<div class="col-sm-6">
    <div class="well bordered-top bordered-palegreen">
        <div class="form-group col-xs-12 {{ $errors->has('name') ? 'has-error':''}}">
            <div class="col-sm-2 text-right">
                {!! Form::label('name','Employee Name',['class'=>'field-required']) !!}
            </div>
            <div class="col-sm-10">
                {!! Form::text('name',null,['class'=>'form-control']) !!}
                {!! $errors->first('name','<span class="help-block">:message</span>') !!}
            </div>
        </div>


        <div class="form-group col-xs-12 {{ $errors->has('old_card_no') ? 'has-error':''}}">
            <div class="col-sm-2 text-right">
                {!! Form::label('old_card_no','Old Card No',['class'=>'field-required']) !!}
            </div>
            <div class="col-sm-10">
                {!! Form::text('old_card_no',null,['class'=>'form-control']) !!}
                {!! $errors->first('old_card_no','<span class="help-block">:message</span>') !!}
                @if(!empty($card_no))<strong><span class="help-block text-danger">Last Card No: {!! $card_no !!}</span></strong>@endif
            </div>
        </div>

        @if(!empty($id))
            <div class="form-group col-xs-12 {{ $errors->has('card_no') ? 'has-error':''}}">
                <div class="col-sm-2 text-right">
                    {!! Form::label('card_no','Card No',['class'=>'field-required']) !!}
                </div>
                <div class="col-sm-10">
                    {!! Form::text('card_no',null,['class'=>'form-control','disabled'=>'disabled']) !!}
                    {!! $errors->first('card_no','<span class="help-block">:message</span>') !!}
                </div>
            </div>
        @endif

        {{--@if(!empty($id))--}}
            <div class="form-group col-xs-12 {{ $errors->has('organization_id') ? 'has-error':''}}">
                <div class="col-sm-2 text-right">
                    {!! Form::label('organization_id','Organization',['class'=>'field-required']) !!}
                </div>
                <div class="col-sm-10">
                    @if(!empty($id))
                        <?php $organization_id = [$id->organization->id => $id->organization->name]  ?>
                    @else
                        <?php $organization_id = []  ?>
                    @endif
                    {!! Form::select('organization_id',$organization_id,null,['class'=>'form-control']) !!}
                    {!! $errors->first('organization_id','<span class="help-block">:message</span>') !!}
                </div>
            </div>


            <div class="form-group col-xs-12 {{ $errors->has('unit_id') ? 'has-error':''}}">
                <div class="col-sm-2 text-right">
                    {!! Form::label('unit_id','Unit',['class'=>'field-required']) !!}
                </div>
                <div class="col-sm-10">
                    @if(!empty($id))
                        <?php $unit_id = [$id->unit->id => $id->unit->name]  ?>
                    @else
                        <?php $unit_id = []  ?>
                    @endif
                    {!! Form::select('unit_id',$unit_id,null,['class'=>'form-control']) !!}
                    {!! $errors->first('unit_id','<span class="help-block">:message</span>') !!}
                </div>
            </div>


            <div class="form-group col-xs-12 {{ $errors->has('division_id') ? 'has-error':''}}">
                <div class="col-sm-2 text-right">
                    {!! Form::label('division_id','Division',['class'=>'field-required']) !!}
                </div>
                <div class="col-sm-10">
                    @if(!empty($id))
                        <?php $division_id = [$id->division->id => $id->division->name]  ?>
                    @else
                        <?php $division_id = []  ?>
                    @endif
                    {!! Form::select('division_id',$division_id,null,['class'=>'form-control']) !!}
                    {!! $errors->first('division_id','<span class="help-block">:message</span>') !!}
                </div>
            </div>


            <div class="form-group col-xs-12 {{ $errors->has('department_id') ? 'has-error':''}}">
                <div class="col-sm-2 text-right">
                    {!! Form::label('department_id','Department',['class'=>'field-required']) !!}
                </div>
                <div class="col-sm-10">
                    @if(!empty($id))
                        <?php $department_id = [$id->department->id => $id->department->name]  ?>
                    @else
                        <?php $department_id = []  ?>
                    @endif
                    {!! Form::select('department_id',$department_id,null,['class'=>'form-control']) !!}
                    {!! $errors->first('department_id','<span class="help-block">:message</span>') !!}
                </div>
            </div>


            <div class="form-group col-xs-12 {{ $errors->has('section_id') ? 'has-error':''}}">
                <div class="col-sm-2 text-right">
                    {!! Form::label('section_id','Section',['class'=>'field-required']) !!}
                </div>
                <div class="col-sm-10">
                    @if(!empty($id))
                        <?php $section_id = [$id->section->id => $id->section->name]  ?>
                    @else
                        <?php $section_id = []  ?>
                    @endif
                    {!! Form::select('section_id',$section_id,null,['class'=>'form-control']) !!}
                    {!! $errors->first('section_id','<span class="help-block">:message</span>') !!}
                </div>
            </div>


            <div class="form-group col-xs-12 {{ $errors->has('line_id') ? 'has-error':''}}">
                <div class="col-sm-2 text-right">
                    {!! Form::label('line_id','Line',['class'=>'field-required']) !!}
                </div>
                <div class="col-sm-10">
                    @if(!empty($id))
                        <?php $line_id = [$id->line->id => $id->line->name]  ?>
                    @else
                        <?php $line_id = []  ?>
                    @endif
                    {!! Form::select('line_id',$line_id,null,['class'=>'form-control']) !!}
                    {!! $errors->first('line_id','<span class="help-block">:message</span>') !!}
                </div>
            </div>


            <div class="form-group col-xs-12 {{ $errors->has('designation_group_id') ? 'has-error':''}}">
                <div class="col-sm-2 text-right">
                    {!! Form::label('designation_group_id','Designation Group',['class'=>'field-required']) !!}
                </div>
                <div class="col-sm-10">
                    @if(!empty($id))
                        <?php $designation_group_id = [$id->designationGroup->id => $id->designationGroup->name]  ?>
                    @else
                        <?php $designation_group_id = []  ?>
                    @endif
                    {!! Form::select('designation_group_id',$designation_group_id,null,['class'=>'form-control']) !!}
                    {!! $errors->first('designation_group_id','<span class="help-block">:message</span>') !!}
                </div>
            </div>
        {{--@endif--}}

        <div class="form-group col-xs-12 {{ $errors->has('designation_id') ? 'has-error':''}}">
            <div class="col-sm-2 text-right">
                {!! Form::label('designation_id','Designation',['class'=>'field-required']) !!}
            </div>
            <div class="col-sm-10">
                @if(!empty($id))
                    <?php $designation_id = [$id->designation->id => $id->designation->name]  ?>
                @else
                    <?php $designation_id = []  ?>
                @endif
                {!! Form::select('designation_id',$designation_id,null,['class'=>'form-control']) !!}
                {!! $errors->first('designation_id','<span class="help-block">:message</span>') !!}
            </div>
        </div>


        <div class="form-group col-xs-12 {{ $errors->has('employee_category_id') ? 'has-error':''}}">
            <div class="col-sm-2 text-right">
                {!! Form::label('employee_category_id','Category',['class'=>'field-required']) !!}
            </div>
            <div class="col-sm-10">
                @if(!empty($id))
                    <?php $employee_category_id = [$id->employeeCategory->id => $id->employeeCategory->name]  ?>
                @else
                    <?php $employee_category_id = []  ?>
                @endif
                {!! Form::select('employee_category_id',$employee_category_id,null,['class'=>'form-control']) !!}
                {!! $errors->first('employee_category_id','<span class="help-block">:message</span>') !!}
            </div>
        </div>


        <div class="form-group col-xs-12 {{ $errors->has('employee_type_id') ? 'has-error':''}}">
            <div class="col-sm-2 text-right">
                {!! Form::label('employee_type_id','Type',['class'=>'field-required']) !!}
            </div>
            <div class="col-sm-10">
                @if(!empty($id))
                    <?php $employee_type_id = [$id->employeeType->id => $id->employeeType->name]  ?>
                @else
                    <?php $employee_type_id = []  ?>
                @endif
                {!! Form::select('employee_type_id',$employee_type_id,null,['class'=>'form-control']) !!}
                {!! $errors->first('employee_type_id','<span class="help-block">:message</span>') !!}
            </div>
        </div>


        <div class="form-group col-xs-12 {{ $errors->has('holiday_id') ? 'has-error':''}}">
            <div class="col-sm-2 text-right">
                {!! Form::label('holiday_id','Holiday List',['class'=>'field-required']) !!}
            </div>
            <div class="col-sm-10">
                @if(!empty($id))
                    <?php $holiday_id = [$id->holiday->id => $id->holiday->name]  ?>
                @else
                    <?php $holiday_id = [] ?>
                @endif
                {!! Form::select('holiday_id',$holiday_id,null,['class'=>'form-control']) !!}
                {!! $errors->first('holiday_id','<span class="help-block">:message</span>') !!}
            </div>
        </div>


        <div class="form-group col-xs-12 {{ $errors->has('probation_period_id') ? 'has-error':''}}">
            <div class="col-sm-2 text-right">
                {!! Form::label('probation_period_id','Probation Period',['class'=>'field-required']) !!}
            </div>
            <div class="col-sm-10">
                @if(!empty($id))
                    <?php $probation_period_id= [$id->probationPeriod->id => $id->probationPeriod->name]  ?>
                @else
                    <?php $probation_period_id= [] ?>
                @endif
                {!! Form::select('probation_period_id',$probation_period_id,null,['class'=>'form-control']) !!}
                {!! $errors->first('probation_period_id','<span class="help-block">:message</span>') !!}
            </div>
        </div>


        <div class="form-group col-xs-12 {{ $errors->has('sex') ? 'has-error':''}}">
            <div class="col-sm-2 text-right">
                {!! Form::label('sex','Sex',['class'=>'field-required']) !!}
            </div>
            <div class="col-sm-10">
                {!! Form::select('sex',['Male'=>'Male','Female'=>'Female','Others'=>'Others',],null,['class'=>'form-control']) !!}
                {!! $errors->first('sex','<span class="help-block">:message</span>') !!}
            </div>
        </div>


        <div class="form-group col-xs-12 {{ $errors->has('status') ? 'has-error':''}}">
            <div class="col-sm-2 text-right">
                {!! Form::label('status','Status',['class'=>'field-required']) !!}
            </div>
            <div class="col-sm-10">
                {!! Form::select('status',['Active'=>'Active','Inactive'=>'Inactive'],null,['class'=>'form-control']) !!}
                {!! $errors->first('status','<span class="help-block">:message</span>') !!}
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<div class="col-sm-6">
    <div class="well bordered-top bordered-palegreen">
        <div class="form-group col-xs-12 {{ $errors->has('date_of_joining') ? 'has-error':''}}">
            <div class="col-sm-2 text-right">
                {!! Form::label('date_of_joining','Date of Joining') !!}
            </div>
            <div class="col-sm-10">
                {!! Form::text('date_of_joining',null,['class'=>'form-control','placeholder'=>'YYYY-MM-DD']) !!}
                {!! $errors->first('date_of_joining','<span class="help-block">:message</span>') !!}
            </div>
        </div>


        <div class="form-group col-xs-12 {{ $errors->has('joining_salary') ? 'has-error':''}}">
            <div class="col-sm-2 text-right">
                {!! Form::label('joining_salary','Joining Salary') !!}
            </div>
            <div class="col-sm-10">
                {!! Form::number('joining_salary',null,['class'=>'form-control']) !!}
                {!! $errors->first('joining_salary','<span class="help-block">:message</span>') !!}
            </div>
        </div>


        <div class="form-group col-xs-12 {{ $errors->has('current_salary') ? 'has-error':''}}">
            <div class="col-sm-2 text-right">
                {!! Form::label('current_salary','Current Salary') !!}
            </div>
            <div class="col-sm-10">
                {!! Form::number('current_salary',null,['class'=>'form-control']) !!}
                {!! $errors->first('current_salary','<span class="help-block">:message</span>') !!}
            </div>
        </div>


        {{--<div class="form-group col-xs-12 {{ $errors->has('last_increment_date') ? 'has-error':''}}">
            <div class="col-sm-2 text-right">
                {!! Form::label('last_increment_date','Last Increment Date') !!}
            </div>
            <div class="col-sm-10">
                {!! Form::text('last_increment_date',null,['class'=>'form-control','placeholder'=>'YYYY-MM-DD']) !!}
                {!! $errors->first('last_increment_date','<span class="help-block">:message</span>') !!}
            </div>
        </div>


        <div class="form-group col-xs-12 {{ $errors->has('increment_month') ? 'has-error':''}}">
            <div class="col-sm-2 text-right">
                {!! Form::label('increment_month','Increment Month') !!}
            </div>
            <div class="col-sm-10">
                {!! Form::number('increment_month',null,['class'=>'form-control']) !!}
                {!! $errors->first('increment_month','<span class="help-block">:message</span>') !!}
            </div>
        </div>


        <div class="form-group col-xs-12 {{ $errors->has('increment_date') ? 'has-error':''}}">
            <div class="col-sm-2 text-right">
                {!! Form::label('increment_date','Increment Date') !!}
            </div>
            <div class="col-sm-10">
                {!! Form::text('increment_date',null,['class'=>'form-control']) !!}
                {!! $errors->first('increment_date','<span class="help-block">:message</span>') !!}
            </div>
        </div>--}}


        <div class="form-group col-xs-12 {{ $errors->has('reason_of_separation') ? 'has-error':''}}">
            <div class="col-sm-2 text-right">
                {!! Form::label('reason_of_separation','Reason of Separation') !!}
            </div>
            <div class="col-sm-10">
                {!! Form::text('reason_of_separation',null,['class'=>'form-control']) !!}
                {!! $errors->first('reason_of_separation','<span class="help-block">:message</span>') !!}
            </div>
        </div>


        <div class="form-group col-xs-12 {{ $errors->has('date_of_separation') ? 'has-error':''}}">
            <div class="col-sm-2 text-right">
                {!! Form::label('date_of_separation','Date of Separation') !!}
            </div>
            <div class="col-sm-10">
                {!! Form::text('date_of_separation',null,['class'=>'form-control']) !!}
                {!! $errors->first('date_of_separation','<span class="help-block">:message</span>') !!}
            </div>
        </div>


        <div class="form-group col-xs-12 {{ $errors->has('confirm_date') ? 'has-error':''}}">
            <div class="col-sm-2 text-right">
                {!! Form::label('confirm_date','Confirm Date') !!}
            </div>
            <div class="col-sm-10">
                {!! Form::text('confirm_date',null,['class'=>'form-control']) !!}
                {!! $errors->first('confirm_date','<span class="help-block">:message</span>') !!}
            </div>
        </div>


        <div class="form-group col-xs-12 {{ $errors->has('payment_mode') ? 'has-error':''}}">
            <div class="col-sm-2 text-right">
                {!! Form::label('payment_mode','Payment Mode') !!}
            </div>
            <div class="col-sm-10">
                {!! Form::select('payment_mode',['Cash'=>'Cash','Bank'=>'Bank'],null,['class'=>'form-control']) !!}
                {!! $errors->first('payment_mode','<span class="help-block">:message</span>') !!}
            </div>
        </div>


        <div class="form-group col-xs-12 {{ $errors->has('bank_name') ? 'has-error':''}}">
            <div class="col-sm-2 text-right">
                {!! Form::label('bank_name','Bank Name') !!}
            </div>
            <div class="col-sm-10">
                {!! Form::text('bank_name',null,['class'=>'form-control']) !!}
                {!! $errors->first('bank_name','<span class="help-block">:message</span>') !!}
            </div>
        </div>


        <div class="form-group col-xs-12 {{ $errors->has('branch_name') ? 'has-error':''}}">
            <div class="col-sm-2 text-right">
                {!! Form::label('branch_name','Branch Name') !!}
            </div>
            <div class="col-sm-10">
                {!! Form::text('branch_name',null,['class'=>'form-control']) !!}
                {!! $errors->first('branch_name','<span class="help-block">:message</span>') !!}
            </div>
        </div>


        <div class="form-group col-xs-12 {{ $errors->has('account_holder') ? 'has-error':''}}">
            <div class="col-sm-2 text-right">
                {!! Form::label('account_holder','Account Holder') !!}
            </div>
            <div class="col-sm-10">
                {!! Form::text('account_holder',null,['class'=>'form-control']) !!}
                {!! $errors->first('account_holder','<span class="help-block">:message</span>') !!}
            </div>
        </div>


        <div class="form-group col-xs-12 {{ $errors->has('account_no') ? 'has-error':''}}">
            <div class="col-sm-2 text-right">
                {!! Form::label('account_no','Account No') !!}
            </div>
            <div class="col-sm-10">
                {!! Form::text('account_no',null,['class'=>'form-control']) !!}
                {!! $errors->first('account_no','<span class="help-block">:message</span>') !!}
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<div class="clearfix"></div>

<div class="well bordered-top bordered-palegreen">
    <h5 class="row-title before-palegreen"><i class="fa fa-columns palegreen"></i>Salary</h5>
    <div class="clearfix"></div>
    <div class="form-group col-xs-12 {{ $errors->has('gross_salary') ? 'has-error':''}}">
        <div class="col-sm-2 text-right">
            {!! Form::label('gross_salary','Gross Salary') !!}
        </div>
        <div class="col-sm-10">
            {!! Form::text('gross_salary',null,['class'=>'form-control']) !!}
            {!! $errors->first('gross_salary','<span class="help-block">:message</span>') !!}
        </div>
    </div>
    <div class="clearfix"></div>

    <table class="table table-bodered">
        <thead>
        <tr>
            <th>Salary Type</th>
            <th>Amount</th>
            <th>Min Amount</th>
            <th>Max Amount</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-right text-success">
                    {!! Form::label('basic_salary','Basic Salary') !!}
                </td>
                <td class="form-group {{ $errors->has('basic_salary') ? 'has-error':''}}">
                    {!! Form::text('basic_salary',null,['class'=>'form-control']) !!}
                    {!! $errors->first('basic_salary','<span class="help-block">:message</span>') !!}
                </td>
                <td>
                    {!! Form::text('min_basic',null,['class'=>'form-control','readonly'=>'readonly']) !!}
                </td>
                <td>
                    {!! Form::text('max_basic',null,['class'=>'form-control','readonly'=>'readonly']) !!}
                </td>
            </tr>
            <tr>
                <td class="text-right text-success">
                    {!! Form::label('hra','House Rent') !!}
                </td>
                <td class="form-group {{ $errors->has('hra') ? 'has-error':''}}">
                    {!! Form::text('hra',null,['class'=>'form-control']) !!}
                    {!! $errors->first('hra','<span class="help-block">:message</span>') !!}
                </td>
                <td>
                    {!! Form::text('min_hra',null,['class'=>'form-control','readonly'=>'readonly']) !!}
                </td>
                <td>
                    {!! Form::text('max_hra',null,['class'=>'form-control','readonly'=>'readonly']) !!}
                </td>
            </tr>
            <tr>
                <td class="text-right text-success">
                    {!! Form::label('medical','Medical') !!}
                </td>
                <td class="form-group {{ $errors->has('medical') ? 'has-error':''}}">
                    @if(!empty($id))
                        {{--*/ $medical = $id->conveyance /*--}}
                    @else
                        {{--*/ $medical = 250 /*--}}
                    @endif
                    {!! Form::text('medical',$medical,['class'=>'form-control']) !!}
                    {!! $errors->first('medical','<span class="help-block">:message</span>') !!}
                </td>
                <td>
                    {!! Form::text('min_medical',null,['class'=>'form-control','readonly'=>'readonly']) !!}
                </td>
                <td>
                    {!! Form::text('max_medical',null,['class'=>'form-control','readonly'=>'readonly']) !!}
                </td>
            </tr>
            <tr>
                <td class="text-right text-success">
                    {!! Form::label('food_all','Food') !!}
                </td>
                <td class="form-group {{ $errors->has('food_all') ? 'has-error':''}}">
                    {!! Form::text('food_all',null,['class'=>'form-control']) !!}
                    {!! $errors->first('food_all','<span class="help-block">:message</span>') !!}
                </td>
                <td>
                    {!! Form::text('min_food',null,['class'=>'form-control','readonly'=>'readonly']) !!}
                </td>
                <td>
                    {!! Form::text('max_food',null,['class'=>'form-control','readonly'=>'readonly']) !!}
                </td>
            </tr>
            <tr>
                <td class="text-right text-success">
                    {!! Form::label('conveyance','Conveyance') !!}
                </td>
                <td class="form-group {{ $errors->has('conveyance') ? 'has-error':''}}">
                    @if(!empty($id))
                        {{--*/ $conveyance = $id->conveyance /*--}}
                    @else
                        {{--*/ $conveyance = 200 /*--}}
                    @endif
                    {!! Form::text('conveyance',$conveyance,['class'=>'form-control']) !!}
                    {!! $errors->first('conveyance','<span class="help-block">:message</span>') !!}
                </td>
                <td>
                    {!! Form::text('min_conveyance',null,['class'=>'form-control','readonly'=>'readonly']) !!}
                </td>
                <td>
                    {!! Form::text('max_conveyance',null,['class'=>'form-control','readonly'=>'readonly']) !!}
                </td>
            </tr>
            <tr>
                <td class="text-right text-success">
                    {!! Form::label('mobile_bill','Mobile Bill') !!}
                </td>
                <td class="form-group {{ $errors->has('mobile_bill') ? 'has-error':''}}">
                    {!! Form::text('mobile_bill',null,['class'=>'form-control']) !!}
                    {!! $errors->first('mobile_bill','<span class="help-block">:message</span>') !!}
                </td>
                <td>
                    {!! Form::text('min_mobile',null,['class'=>'form-control','readonly'=>'readonly']) !!}
                </td>
                <td>
                    {!! Form::text('max_mobile',null,['class'=>'form-control','readonly'=>'readonly']) !!}
                </td>
            </tr>
            <tr>
                <td class="text-right text-success">
                    {!! Form::label('special_bill','Special Bill') !!}
                </td>
                <td class="form-group {{ $errors->has('special_bill') ? 'has-error':''}}">
                    {!! Form::text('special_bill',null,['class'=>'form-control']) !!}
                    {!! $errors->first('special_bill','<span class="help-block">:message</span>') !!}
                </td>
                <td>
                    {!! Form::text('min_special',null,['class'=>'form-control','readonly'=>'readonly']) !!}
                </td>
                <td>
                    {!! Form::text('max_special',null,['class'=>'form-control','readonly'=>'readonly']) !!}
                </td>
            </tr>
            <tr>
                <td class="text-right text-success">
                    {!! Form::label('car_bill','Car Bill') !!}
                </td>
                <td class="form-group {{ $errors->has('car_bill') ? 'has-error':''}}">
                    {!! Form::text('car_bill',null,['class'=>'form-control']) !!}
                    {!! $errors->first('car_bill','<span class="help-block">:message</span>') !!}
                </td>
                <td>
                    {!! Form::text('min_car',null,['class'=>'form-control','readonly'=>'readonly']) !!}
                </td>
                <td>
                    {!! Form::text('max_car',null,['class'=>'form-control','readonly'=>'readonly']) !!}
                </td>
            </tr>
            <tr>
                <td class="text-right text-success">
                    {!! Form::label('others','Others') !!}
                </td>
                <td class="form-group {{ $errors->has('others') ? 'has-error':''}}">
                    {!! Form::text('others',null,['class'=>'form-control']) !!}
                    {!! $errors->first('others','<span class="help-block">:message</span>') !!}
                </td>
                <td>
                    {!! Form::text('min_others',null,['class'=>'form-control','readonly'=>'readonly']) !!}
                </td>
                <td>
                    {!! Form::text('max_others',null,['class'=>'form-control','readonly'=>'readonly']) !!}
                </td>
            </tr>
            <tr>
                <td class="text-right text-success">
                    {!! Form::label('incentive','Incentive') !!}
                </td>
                <td class="form-group {{ $errors->has('incentive') ? 'has-error':''}}">
                    {!! Form::text('incentive',null,['class'=>'form-control']) !!}
                    {!! $errors->first('incentive','<span class="help-block">:message</span>') !!}
                </td>
                <td>
                    {!! Form::text('min_incentive',null,['class'=>'form-control','readonly'=>'readonly']) !!}
                </td>
                <td>
                    {!! Form::text('max_incentive',null,['class'=>'form-control','readonly'=>'readonly']) !!}
                </td>
            </tr>
        </tbody>
    </table>
    <div class="clearfix"></div>
</div>