<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 8/10/16
 * Time: 11:55 PM
 */
?>
<tr>
    <td>{!! $sl !!}</td>
    <td>@if($row->employee){!! $row->employee->name !!}@endif</td>
    <td>{!! $row->p_start_date !!}</td>
    <td>{!! $row->p_end_date !!}</td>
    <td>{!! $row->p_days !!}</td>
    <td>
        {{--@if($row->deatails)--}}
        @foreach($row->details as $leaveType)
            <p>{!! $leaveType->leaveType->name !!} <spna class="pull-right">{!! $leaveType->days !!}</spna></p>
        @endforeach
        {{--@endif--}}
    </td>
    <td>{!! $row->createdBy->name !!}</td>
    <td>
        @if($row->updateBy)
            {!! $row->updateBy->name !!}
        @endif
    </td>
    <td class="text-center">
        <a href="{!! action('LeaveRegisterController@show',$row->id) !!}"><i class="fa fa-eye"></i></a>
        @if($row->is_permanent_save !="Yes")
            <a href="{!! action('LeaveRegisterController@edit',$row->id) !!}" ><i class="fa fa-pencil-square-o text-danger"></i></a>
            {!! delete_data('LeaveRegisterController@destroy',$row->id) !!}
        @endif
    </td>
</tr>
